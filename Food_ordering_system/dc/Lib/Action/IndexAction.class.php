<?php
// 本文档自动生成，仅供测试运行
class IndexAction extends Action
{
    /**
    +----------------------------------------------------------
    * 默认操作
    +----------------------------------------------------------
    */

	function _initialize()
	{	
		 header('Content-Type:text/html; charset=utf-8');//防止出现乱码
		 if($_SESSION['type'])
		 {
		//	unset($_SESSION[C('USER_AUTH_KEY')]);
		//	unset($_SESSION);
		//	session_destroy();
		 }

	}
//注册插入开始	
public function insert()
{
//session_start();
$_SESSION['login']=false;
$DB=D('student');
$sid=$_POST['sid'];
$name=$_POST['sname'];
$passwd=$_POST['spassword'];
$position=$_POST['sposition'];
$phone=$_POST['sphone'];
$sregip=getenv(REMOTE_ADDR);
$sregtime=date("Y-m-d H:i:s");
if(!$passwd)
{
echo "密码不能为空！";
$this->redirect('profile');
}

$map=array();
$map['sid']=$sid;
$list=$DB->where($map)->find();
if($list)
{
echo "这个学号已经注册!";
$this->redirect('profile');
}
else
{
$map1['sid']=$sid;
$map1['sname']=$name;
$map1['spassword']=$passwd;
$map1['sposition']=$position;
$map1['sphone']=$phone;
$map1['sregip']=getenv(REMOTE_ADDR);
$map1['sregtime']=date("Y-m-d H:i:s");
//dump($map1);
$result=$DB->add($map1);
if($result>0){
$this->assign("jumpUrl",__URL__.'/index');
$this->success('注册成功');}
}

//session_destroy();
}
//注册插入结束

//验证用户名密码开始
public function checkUser()
{
	session_start();
    $_SESSION['login']=false;
    $DB=D('student');
    $usernum=$_POST['sid'];
    $pwd=$_POST['spassword'];
     if (empty($usernum))
    {
       echo "input your name:";
      $this->redirect('Index/','',0,'');
    }
     elseif (empty($pwd))
    {
     echo "input yor passwd:";
      $this->redirect('Index/','',0,'');
    }
     $map=array(); 
     $map['sid']=$usernum;
     $map['spassword']=$pwd;
     $list=$DB->where($map)->find();
        if(!$list)   
      {
         echo "用户名或密码错误！"; $this->redirect('Index/','',0,''); 
      } 
        else    
       {       
       $_SESSION['sid']=$list['sid'];	
	   $_SESSION['sname']=$list['sname'];
       $_SESSION['spassword']=$list['spassword'];
       $_SESSION['sposition']=$list['sposition'];
 	   $_SESSION['sphone']=$list['sphone'];
       $_SESSION['login']=true;
       $_SESSION[C('USER_AUTH_KEY')]=$list['sname'];
       $this->assign("jumpUrl",__URL__.'/info');
       $this->success('success!');
       }
}
//验证结束

//退出开始
 public function logout ()
{
if(isset($_SESSION[C('USER_AUTH_KEY')])) 
{
  unset($_SESSION[C('USER_AUTH_KEY')]);
  unset($_SESSION);
  session_destroy();
 // $this->assign("jumpUrl",__URL__.'/index');
//  $this->success('success out');
$this->redirect('index',array('cate_id'=>2),0,'byebye');
//$this->display();

}
 else {echo "wrong"; $this->redirect('Index/','',0,'');}
}
//退出结束
 public function stu_info ()
 {
	 session_start();
	 $DB=M('student');
	 $condition['sid']=$_SESSION['sid'];
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $this->assign('list',$list);
	 $this->display();
	 }
	 //显示信息开始
	/* public function stu_info()
{
if(!Session::is_set('sname'))  
//$_SESSION[C('USER_AUTH_KEY')]=$_SESSION['numble'];
//if(!isset($_SESSION[C('USER_AUTH_KEY')]))
{
$this->redirect('Index/','',0,'');
}
else 
{
 $DB=M('student');
// echo $_SESSION['numble'];
// echo $_SESSION['SID'];
 $condition->sid=$_SESSION['sid'];
 $list=$DB->where($condition)->select();
 //dump($list);
 $this->assign('list',$list);
 $this->display();

}
}*/
//显示信息结束

//修改密码开始
public function passwdVerify(){
	session_start();
	$DB=M('student');
	$p=$_POST['oldpasswd'];
	$p1=$_POST['newpasswd1'];
	$p2=$_POST['newpasswd2'];
	if($_SESSION['spassword']==$p){
		if($p1==$p2){
			$data['spassword']=$p1;
			$data['sid']=$_SESSION['sid'];
			$result=$DB->save($data);
					if($result!==false){
					$_SESSION['spassword']=$p1;
					$this->redirect('stu_info','', 2, '密码修改成功');
					}
					else{ $this->redirect('changepwd','', 2, 'sorry，密码修改失败');
					}
		}
		else { $this->redirect('changepwd','', 2, 'sorry，两次密码输入不一致');}
	}		
	else $this->redirect('changepwd','', 2, '对不起，您的密码错误');
	
	}
	//修改密码结束
	
	//修改个人信息开始
	public function changeinformation()
	{
		session_start();
		$DB=M('student');
		$p=$_POST['phone'];
		$q=$_POST['address'];
		if(!$p)
			$data['sphone']=$_SESSION['sphone'];
		else{
			$data['sphone']=$p;
			$_SESSION['sphone']=$p;}
		if(!$q)
			$data['sposition']=$_SESSION['sposition'];
		else{
			$data['sposition']=$q;
			$_SESSION['sposition']=$q;}
		$data['sid']=$_SESSION['sid'];
		$result=$DB->save($data);
				if($result!==false){
					$this->redirect('stu_info','', 2, '信息修改成功');
					}
		}
		
	//修改个人信息结束
	
	/*自动生成总订单开始
	public function autoorderall()
	{
	// session_start();
	 $MB=M('student'); $DB=D('order');
	 $condition['sid']=$_SESSION['sid'];
	 $list=$MB->where($condition)->select();
	 dump($list);
	 
	
	 $map2=array();
	 $map2['osid']=$_SESSION['sid'];
	 $map2['oaddress']=$_SESSION['sposition'];
	 $map2['oordertime']=date("Y-m-d H:i:s");
	 dump($map2);
	 $result=$DB->add($map2);
	$this->redirect('orderall','', 10, '总订单生成成功');

	}
	//自动生成总订单结束*/
	
	//获取同州信息
	public function getmenutongzhou()
	{
	 session_start();
	 $DB=M('menu');$MB=M('restaurant');
	 $condition['mrestid']="同州食坊";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $condition2['rname']="同州食坊";
	 $list2=$MB->where($condition2)->find();
	 $address=$list2['raddress'];
	 $phone=$list2['rphone'];
	 $info=$list2['rinfo'];
	 $this->assign("address",$address);
	 $this->assign("phone",$phone);
	 $this->assign("info",$info);
	 $this->assign('list',$list);
	 $this->display();
	
		}
		//获取成都信息
	public function getmenuchengdu()
	{
	 session_start();
	 $DB=M('menu');$MB=M('restaurant');
	 $condition['mrestid']="成都快餐";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $condition2['rname']="成都快餐";
	 $list2=$MB->where($condition2)->find();
	 $address=$list2['raddress'];
	 $phone=$list2['rphone'];
	 $info=$list2['rinfo'];
	 $this->assign("address",$address);
	 $this->assign("phone",$phone);
	 $this->assign("info",$info);
	 $this->assign('list',$list);
	 $this->display();
		}
		//获取川湘信息
	public function getmenuchuanxiang()
	{
	 session_start();
	 $DB=M('menu');$MB=M('restaurant');
	 $condition['mrestid']="川湘会馆";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $condition2['rname']="川湘会馆";
	 $list2=$MB->where($condition2)->find();
	 $address=$list2['raddress'];
	 $phone=$list2['rphone'];
	 $info=$list2['rinfo'];
	 $this->assign("address",$address);
	 $this->assign("phone",$phone);
	 $this->assign("info",$info);
	 $this->assign('list',$list);
	 $this->display();
		}
		//获取心悦信息
		public function getmenuxinyue()
	{
	 session_start();
	 $DB=M('menu');$MB=M('restaurant');
	 $condition['mrestid']="心悦盖浇饭";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	  $condition2['rname']="心悦盖浇饭";
	 $list2=$MB->where($condition2)->find();
	 $address=$list2['raddress'];
	 $phone=$list2['rphone'];
	 $info=$list2['rinfo'];
	 $this->assign("address",$address);
	 $this->assign("phone",$phone);
	 $this->assign("info",$info);
	 $this->assign('list',$list);
	 $this->display();
	
		}	
		//获取兄弟信息
		public function getmenuxiongdi()
	{
	 session_start();
	 $DB=M('menu');$MB=M('restaurant');
	 $condition['mrestid']="兄弟锅巴饭";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $condition2['rname']="兄弟锅巴饭";
	 $list2=$MB->where($condition2)->find();
	 $address=$list2['raddress'];
	 $phone=$list2['rphone'];
	 $info=$list2['rinfo'];
	 $this->assign("address",$address);
	 $this->assign("phone",$phone);
	 $this->assign("info",$info);
	 $this->assign('list',$list);
	 $this->display();
	
		} 
		//测试换页
		public function changepage()
		{
			$s=$_POST['restaurant'];
			if($s=="同州食坊")
			$this->redirect('ordertongzhou','',0,'');

			}
		//已经不再使用
		public function ordertongzhou()
	{
	 session_start();
	 $DB=M('menu');
	 $condition['mrestid']="同州食坊";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $this->assign('list',$list);
	 $this->display();
		}
		
	/*	
		public function order()
		{
	 session_start();
	 $MB=M('student');$NB=M('menu'); $DB=D('order');
	 $flag=$_POST['{$vo.mid}']; 
	 if($flag=='true'){
	 $number=$_POST['{$vo.mname}'];}
	 dump($_POST);

	 $map2=array();
	 $map2['osid']=$_SESSION['sid'];
	 $map2['ofood']=
	 $map2['onumber']=
	 $map2['oordertime']=date("Y-m-d H:i:s");
	 $map2['oaddress']=$_SESSION['sposition'];
	 //dump($map2);
	 $result=$DB->add($map2);

			}*/
			
			//订餐开始
			public function order2()
			{
				$DB=M('menu');
				$condition['mid']=$_GET['mid'];
				$data=$DB->where($condition)->find();
				$name=$data['mname'];
				$price=$data['mprice'];
				$this->assign("name",$name);
				$this->assign("price",$price);
				$this->assign("mid",$_GET['mid']);
				$this->display();
				}
				
public function ordersubmit(){
	session_start();
	$DB=M('order');$MB=M('menu');
	$map['osid']=$_SESSION['sid'];
	$id=$_GET['mid'];
	$condition['mid']=$id;
	$list=$MB->where($condition)->find();
	$map['ofood']=$list['mname'];
	$map['orest']=$list['mrestid'];
	$map['onumber']=$_POST['select'];
	$map['oprice']=$list['mprice']*$map['onumber'];
	$map['ophone']=$_SESSION['sphone'];
	$map['oordertime']=date("Y-m-d H:i:s");
	$map['oaddress']=$_SESSION['sposition'];
	$result=$DB->add($map);
	if($result){
	$this->redirect('viewlist','', 2, '订餐成功');
	}
	else{ $this->error('订餐失败');
		$this->display('123');
			}
	}
	//订餐结束
	
	//查看本次订餐开始
public function viewlist()
{
	session_start();
	$DB=M('order');$MB=M('student');
	$condition['osid']=$_SESSION['sid'];
	$_SESSION['oordertime']=date("Y-m-d H:i:s");
	$s1=date("Y-m-d H:i:s",strtotime('+1 hour'));//dump($s1);
	$s2=date("Y-m-d H:i:s",strtotime('-1 hour'));//dump($s2);
	$condition['oordertime']=array(array('gt',$s2),array('lt',$s1),'and');
	$list=$DB->where($condition)->select();
	$result=0;
	$num=count($list);
	for($i=0;$i<$num;$i++)
	{
		$price=$list[$i]['oprice'];$result=$result+$price;
		}

	$this->assign("result",$result);
	$this->assign('list',$list);
	$this->display();
	}
	//查看本次订单结束
	
	//查看总订单开始
	public function viewlistall()
{
	session_start();
	$DB=M('order');$MB=M('student');
	$condition['osid']=$_SESSION['sid'];
	$_SESSION['oordertime']=date("Y-m-d H:i:s");
	//$s1=date("Y-m-d H:i:s",strtotime('+1 hour'));//dump($s1);
	//$s2=date("Y-m-d H:i:s",strtotime('-1 hour'));//dump($s2);
	//$condition['oordertime']=array(array('gt',$s2),array('lt',$s1),'and');
	$list=$DB->where($condition)->select();
	$result=0;
	$num=count($list);
	for($i=0;$i<$num;$i++)
	{
		$price=$list[$i]['oprice'];$result=$result+$price;
		}

	$this->assign("result",$result);
	$this->assign('list',$list);
	$this->display();
	}
//查看总订单结束

//写评论开始
public function commentsubmit()
{
	$DB=D('comment');
	$restaurant=$_POST['restaurant'];
	if(!$_POST['comment']){
		$this->redirect('comment','', 2, '评论失败');
		}
	$comment=$_POST['comment'];
	$map=array();
	$map['csid']=$_SESSION['sid'];
	$map['crid']=$restaurant;
	$map['ccomment']=$comment;
	$map['ctime']=date("Y-m-d H:i:s");
	$result=$DB->add($map);
	if($result){
	$this->redirect('viewcomment','', 2, '评论成功');
	}
	else{
	$this->redirect('comment','', 2, '评论失败');
	}
	}//写评论结束
	//查看评论开始
	public function viewcomment()
	{
		$DB=M('comment');
		$list=$DB->select();
		$this->assign('list',$list);
		$this->display();
		}

}

?>
