<?php
// 本文档自动生成，仅供测试运行
class BackAction extends Action
{
    /**
    +----------------------------------------------------------
    * 默认操作
    +----------------------------------------------------------
    */

	function _initialize()
	{	
		 header('Content-Type:text/html; charset=utf-8');//防止出现乱码
		 if($_SESSION['type'])
		 {
		//	unset($_SESSION[C('USER_AUTH_KEY')]);
		//	unset($_SESSION);
		//	session_destroy();
		 }

	}
	
	//验证用户名密码开始
public function checkUser()
{
	session_start();
    $_SESSION['login']=false;
    $DB=D('admin');
    $usernum=$_POST['aid'];
    $pwd=$_POST['apassword'];
     if (empty($usernum))
    {
       echo "input your name:";
      $this->redirect('Index/','',0,'');
    }
     elseif (empty($pwd))
    {
     echo "input yor passwd:";
      $this->redirect('Index/','',0,'');
    }
     $map=array(); 
     $map['aid']=$usernum;
     $map['apassword']=$pwd;
     $list=$DB->where($map)->find();
        if(!$list)   
      {
         echo "用户名或密码错误！"; $this->redirect('Index/','',0,''); 
      } 
        else    
       {       
       $_SESSION['aid']=$list['aid'];	
	   $_SESSION['aname']=$list['aname'];
       $_SESSION['apassword']=$list['apassword'];
       $_SESSION['arest']=$list['srest'];
 	   $_SESSION['aphone']=$list['aphone'];
       $_SESSION['login']=true;
       $_SESSION[C('USER_AUTH_KEY')]=$list['aname'];
       $this->assign("jumpUrl",__URL__.'/info');
       $this->success('success!');
       }
}
//验证结束


//退出开始
 public function logout ()
{
if(isset($_SESSION[C('USER_AUTH_KEY')])) 
{
  unset($_SESSION[C('USER_AUTH_KEY')]);
  unset($_SESSION);
  session_destroy();
 // $this->assign("jumpUrl",__URL__.'/index');
//  $this->success('success out');
$this->redirect('index',array('cate_id'=>2),0,'byebye');
//$this->display();

}
 else {echo "wrong"; $this->redirect('Index/','',0,'');}
}
//退出结束
public function admin_info ()
 {
	 session_start();
	 $DB=M('admin');
	 $condition['aid']=$_SESSION['aid'];
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $this->assign('list',$list);
	 $this->display();
}
public function passwdVerify(){
	session_start();
	$DB=M('admin');
	$p=$_POST['oldpasswd'];
	$p1=$_POST['newpasswd1'];
	$p2=$_POST['newpasswd2'];
	if($_SESSION['apassword']==$p){
		if($p1==$p2){
			$data['apassword']=$p1;
			$data['aid']=$_SESSION['aid'];
			$result=$DB->save($data);
					if($result!==false){
					$_SESSION['apassword']=$p1;
					$this->redirect('admin_info','', 2, '密码修改成功');
					}
					else{ $this->redirect('changepwd','', 2, 'sorry，密码修改失败');
					}
		}
		else { $this->redirect('changepwd','', 2, 'sorry，两次密码输入不一致');}
	}		
	else $this->redirect('changepwd','', 2, '对不起，您的密码错误');
	
	}
	
	public function changeinformation()
	{
		session_start();
		$DB=M('admin');
		$p=$_POST['phone'];
		$q=$_POST['rest'];
		$t=$_POST['info'];

			$data['aphone']=$p;
			$_SESSION['aphone']=$p;
			
			$data['arest']=$q;
			$_SESSION['arest']=$q;
			
		$data['aid']=$_SESSION['aid'];
		$result=$DB->save($data);
				if($result!==false){
					$this->redirect('admin_info','', 2, '信息修改成功');
					}
		}
		
	public function rest_info()
	{
		session_start();
		$DB1=M('admin');
		$DB2=M('restaurant');
		$condition1['aid']=$_SESSION['aid'];
		$list1=$DB1->where($condition1)->find();
		$t=$list1['arest'];
		$condition2['rname']=$t;
		$list2=$DB2->where($condition2)->find();
		$_SESSION['rid']=$list2[rid];
		$_SESSION['rname']=$list2[rname];
		$_SESSION['raddress']=$list2[raddress];
		$_SESSION['rphone']=$list2[rphone];
		$_SESSION['rinfo']=$list2[rinfo];
		$this->display();
		
		
	}
	public function changeinformation_rest()
	{
		session_start();
		$DB=M('restaurant');
		$p=$_POST['phone'];
		$q=$_POST['address'];
		$t=$_POST['info'];
		$data['rphone']=$p;
		$_SESSION['rphone']=$p;	
		$data['raddress']=$q;
		$_SESSION['raddress']=$q;
		$data['rinfo']=$t;
		$_SESSION['rinfo']=$t;
		$data['rid']=$_SESSION['rid'];
		$result=$DB->save($data);
				if($result!==false){
					$this->redirect('rest_info','', 2, '信息修改成功');
					}
	}
	//获取同州信息
	public function getmenutongzhou()
	{
	 session_start();
	 $DB=M('menu');$MB=M('restaurant');
	 $condition['mrestid']="同州食坊";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $condition2['rname']="同州食坊";
	 $list2=$MB->where($condition2)->find();
	 $address=$list2['raddress'];
	 $phone=$list2['rphone'];
	 $info=$list2['rinfo'];
	 $this->assign("address",$address);
	 $this->assign("phone",$phone);
	 $this->assign("info",$info);
	 $this->assign('list',$list);
	 $this->display();
	
		}
		//获取成都信息
	public function getmenuchengdu()
	{
	 session_start();
	 $DB=M('menu');$MB=M('restaurant');
	 $condition['mrestid']="成都快餐";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $condition2['rname']="成都快餐";
	 $list2=$MB->where($condition2)->find();
	 $address=$list2['raddress'];
	 $phone=$list2['rphone'];
	 $info=$list2['rinfo'];
	 $this->assign("address",$address);
	 $this->assign("phone",$phone);
	 $this->assign("info",$info);
	 $this->assign('list',$list);
	 $this->display();
		}
		//获取川湘信息
	public function getmenuchuanxiang()
	{
	 session_start();
	 $DB=M('menu');$MB=M('restaurant');
	 $condition['mrestid']="川湘会馆";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $condition2['rname']="川湘会馆";
	 $list2=$MB->where($condition2)->find();
	 $address=$list2['raddress'];
	 $phone=$list2['rphone'];
	 $info=$list2['rinfo'];
	 $this->assign("address",$address);
	 $this->assign("phone",$phone);
	 $this->assign("info",$info);
	 $this->assign('list',$list);
	 $this->display();
		}
		//获取心悦信息
		public function getmenuxinyue()
	{
	 session_start();
	 $DB=M('menu');$MB=M('restaurant');
	 $condition['mrestid']="心悦盖浇饭";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	  $condition2['rname']="心悦盖浇饭";
	 $list2=$MB->where($condition2)->find();
	 $address=$list2['raddress'];
	 $phone=$list2['rphone'];
	 $info=$list2['rinfo'];
	 $this->assign("address",$address);
	 $this->assign("phone",$phone);
	 $this->assign("info",$info);
	 $this->assign('list',$list);
	 $this->display();
	
		}	
		//获取兄弟信息
		public function getmenuxiongdi()
	{
	 session_start();
	 $DB=M('menu');$MB=M('restaurant');
	 $condition['mrestid']="兄弟锅巴饭";
	 $list=$DB->where($condition)->select();
	 //dump($list);
	 $condition2['rname']="兄弟锅巴饭";
	 $list2=$MB->where($condition2)->find();
	 $address=$list2['raddress'];
	 $phone=$list2['rphone'];
	 $info=$list2['rinfo'];
	 $this->assign("address",$address);
	 $this->assign("phone",$phone);
	 $this->assign("info",$info);
	 $this->assign('list',$list);
	 $this->display();
	
		} 
		
		//查看当天订单开始
public function viewlist()
{
	session_start();
	$DB=M('order');$MB=M('admin');
	$condition2['aid']=$_SESSION['aid'];
	$list2=$MB->where($condition2)->find();
	$condition['orest']=$list2['arest'];
	$_SESSION['oordertime']=date("Y-m-d H:i:s");
	$s1=date("Y-m-d H:i:s",strtotime('+1 hour'));//dump($s1);
	$s2=date("Y-m-d H:i:s",strtotime('-1 hour'));//dump($s2);
	$condition['oordertime']=array(array('gt',$s2),array('lt',$s1),'and');
	$list=$DB->where($condition)->select();

	$result=0;
	$num=count($list);
	for($i=0;$i<$num;$i++)
	{
		$price=$list[$i]['oprice'];$result=$result+$price;
		}

	$this->assign("result",$result);
	$this->assign('list',$list);
	$this->display();
	}
	//查看当天订单结束
	public function viewlistall()
{
	session_start();
	$DB=M('order');$MB=M('admin');
	$condition2['aid']=$_SESSION['aid'];
	$list2=$MB->where($condition2)->find();
	$condition['orest']=$list2['arest'];
	$_SESSION['oordertime']=date("Y-m-d H:i:s");
	//$s1=date("Y-m-d H:i:s",strtotime('+1 hour'));//dump($s1);
	//$s2=date("Y-m-d H:i:s",strtotime('-1 hour'));//dump($s2);
	//$condition['oordertime']=array(array('gt',$s2),array('lt',$s1),'and');
	$list=$DB->where($condition)->select();

	$result=0;
	$num=count($list);
	for($i=0;$i<$num;$i++)
	{
		$price=$list[$i]['oprice'];$result=$result+$price;
		}

	$this->assign("result",$result);
	$this->assign('list',$list);
	$this->display();
	}
	
	public function viewcomment()
	{
	session_start();
	$MB=M('admin');$DB=M('comment');
	$condition['aid']=$_SESSION['aid'];
	$list=$MB->where($condition)->find();
	$condition2['crid']=$list['arest'];
	$list2=$DB->where($condition2)->select();
	$this->assign('list',$list2);
	$this->display();
	
	
	}
}

