<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Accordion Menu Using jQuery</title>
<script type="text/javascript" language="javascript" src="__PUBLIC__/js/jquery.js"></script>
<script type="text/javascript">
<!--//---------------------------------+
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
// --------------------------------->
$(document).ready(function()
{
	//slides the element with class "menu_body" when paragraph with class "menu_head" is clicked 
	$("#firstpane p.menu_head").click(function()
    {
		$(this).css({backgroundImage:"url(down.png)"}).next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
       	$(this).siblings().css({backgroundImage:"url(__PUBLIC__/images/left.png)"});
	});
	//slides the element with class "menu_body" when mouse is over the paragraph
	$("#secondpane p.menu_head").mouseover(function()
    {
	     $(this).css({backgroundImage:"url(down.png)"}).next("div.menu_body").slideDown(500).siblings("div.menu_body").slideUp("slow");
         $(this).siblings().css({backgroundImage:"url(__PUBLIC__/images/left.png)"});
	});
});
</script>
<style type="text/css">
body {
	margin: 10px auto;
	font: 75%/120% Verdana,Arial, Helvetica, sans-serif;
}
.menu_list {	
	width: 200px;
}
.menu_head {
	padding: 5px 10px;
	cursor: pointer;
	position: relative;
	margin:1px;
    font-weight:bold;
    background: #eef4d3 url(left.png) center right no-repeat;
}
.menu_body {
	display:none;
}
.menu_body a{
  display:block;
  color:#006699;
  background-color:#EFEFEF;
  padding-left:10px;
  font-weight:bold;
  text-decoration:none;
}
.menu_body a:hover{
  color: #000000;
  text-decoration:underline;
  }
</style>
</head>
<body>
<div style="float:left" > <!--This is the first division of left-->

  </div>  <!--Code for menu ends here--></div>
<div style="float:left; margin-left:20px;"> <!--This is the second division of right-->
  <p><strong><H1>开始订餐 </h1></strong></p>
<div class="menu_list" id="secondpane"> <!--Code for menu starts here-->
		<p class="menu_head">同州食坊</p>
		<div class="menu_body">
		<a href="__URL__/order2/mid/1">糖醋里脊</a>
        <a href="__URL__/order2/mid/2">千里传香</a>
         <a href="__URL__/order2/mid/3">干煸肥肠</a>
         <a href="__URL__/order2/mid/4">盘龙茄子</a>
         <a href="__URL__/order2/mid/5">椒盐蘑菇</a>
         <a href="__URL__/order2/mid/6">日本豆腐</a>
         <a href="__URL__/order2/mid/7">金针菇炒肉</a>
		</div>
		<p class="menu_head">成都快餐</p>
		<div class="menu_body">
		<a href="__URL__/order2/mid/8">红烧牛肉</a>
         <a href="__URL__/order2/mid/9">辣子鸡块</a>
         <a href="__URL__/order2/mid/10">干炸小鱼</a>
         <a href="__URL__/order2/mid/11">干煸豆角</a>
         <a href="__URL__/order2/mid/12">鱼香茄子</a>
         <a href="__URL__/order2/mid/13">番茄鸡蛋</a>
         <a href="__URL__/order2/mid/14">凉调黄瓜</a>	
		</div>
		<p class="menu_head">川湘会馆</p>
		<div class="menu_body">
          <a href="__URL__/order2/mid/15">港式鸡排</a>
         <a href="__URL__/order2/mid/16">贵妃鸡翅</a>
         <a href="__URL__/order2/mid/17">菠萝鸡片</a>		
		 <a href="__URL__/order2/mid/18">金沙玉米</a>			 
       </div>
       <p class="menu_head">心悦盖浇饭</p>
		<div class="menu_body">
          <a href="__URL__/order2/mid/19">肉末粉条</a>
         <a href="__URL__/order2/mid/20">香干小炒</a>
         <a href="__URL__/order2/mid/21">火爆鱿鱼</a>		
	     <a href="__URL__/order2/mid/22">白菜炒肉</a>		 
       </div>
       <p class="menu_head">兄弟锅巴饭</p>
		<div class="menu_body">
          <a href="__URL__/order2/mid/23">木耳肉片</a>
         <a href="__URL__/order2/mid/24">辣子鸡丁</a>
         <a href="__URL__/order2/mid/25">苜蓿炒肉</a>		
			<a href="__URL__/order2/mid/26">红烧茄子</a>		 
       </div>
  </div>
     <!--Code for menu ends here-->
</div>
<div style="float:left; margin-left:20px;"> <!--This is the second division of right-->

  <p><strong><h2><?php echo $name; ?></h2></strong></p>
<div class="menu_list" id="secondpane">
  <div>
  <form action="__URL__/ordersubmit/mid/<?php echo $mid; ?>" method="post">
  <tr>
  <td>价格：</td>
  <td><?php echo $price; ?></td>
  </tr>
  <tr>
  <td>数量：</td>
  <td><select name="select">
    <option selected="selected">1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
    <option>5</option>
    <option>6</option>
    <option>7</option>
    <option>8</option>
  </select></td>
  
  </tr><tr><td></td>
  <td><input type="submit" value="确定"></td></tr>
  </div>
  </form></div>  
</div>


</body>
</html>