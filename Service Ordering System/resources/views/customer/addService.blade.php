<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<title>Schedule Your Services</title>
<link rel="stylesheet" media="screen" href="css/service.css" />

</head>


<body onLoad="MyAutoRun(); MyAutoRun2()">

@if(session('status'))
	<script>
		alert("Your services have been submitted. And you will receive an confimation email with details!");
	</script>
@endif

{!! Form::open(['id'=>'msform','url'=>'/customer/services']) !!}

	<!-- progressbar -->
	<ul id="progressbar">
		<li class="active">Account Login</li>
		<li>Services</li>
		<li>Additional Information</li>
	</ul>

	<!-- fieldsets -->
	<fieldset id="add">
		<?php
		if(isset($user)){
			echo "Hello, ".$user['firstname']." ".$user['lastname'];
			echo "<br/><br/>";
			echo "
					<script type=\"text/javascript\">
					function MyAutoRun(){

						document.getElementById('modaltrigger').remove();
						document.getElementById('dummy2').remove();
						document.getElementById('modaltrigger2').remove();
						document.getElementById('dummy4').remove();

						var para=document.createElement(\"h3\");
						var node=document.createTextNode(\"Welcome To Our System \");
						para.appendChild(node);
						var element=document.getElementById(\"add\");
						var child=document.getElementById(\"dummy1\");
						element.replaceChild(para, child);

						var btn=document.createElement('input');
						btn.setAttribute('type', 'button');
						btn.setAttribute('class', 'logout-button');
						btn.setAttribute('value', 'Logout');
						btn.setAttribute('onclick','logout()');
						element.appendChild(btn);
					}
					</script>
				";
			}

		?>
		<h2 class="fs-title" id="dummy1">Please Login</h2>
		<a href="#loginmodal" class="flatbtn" id="modaltrigger">Login</a><br>
		<h3 class="fs-subtitle" id="dummy2">Don't have an account?</h3>
		<a href="#registermodal" class="flatbtn" id="modaltrigger2">Register</a><br>
		<h3 class="fs-subtitle" id="dummy4">Continue as Guest? Press Next</h3>
		<input type="button" name="next" class="next action-button" value="Next" id="before"/>

	</fieldset>
	<fieldset >
		<h2 class="fs-title">Choose Your Services</h2>
		<div>
			<table id="services">
				<tr data-tt-id="1">
					<td><a href="">Computer Services</a></td>
				</tr>
				<tr data-tt-id="1-1" data-tt-parent-id="1">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Computer Setup">Computer Setup
					</td>
				</tr>
				<tr data-tt-id="1-2" data-tt-parent-id="1">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Computer Tune-up">Computer Tune-up
					</td>
				</tr>
				<tr data-tt-id="1-3" data-tt-parent-id="1">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Data Backup">Data Backup
					</td>
				</tr>
				<tr data-tt-id="1-4" data-tt-parent-id="1">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Email Setup">Email Setup
					</td>
				</tr>
				<tr data-tt-id="1-5" data-tt-parent-id="1">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Hardware Installation">Hardware Installation
					</td>
				</tr>
				<tr data-tt-id="1-6" data-tt-parent-id="1">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Memory Installation">Memory Installation
					</td>
				</tr>
				<tr data-tt-id="1-7" data-tt-parent-id="1">
					<td >
						<input style="width:20px" type="checkbox" name="service[]" value="Windows Operating System Installation">Windows Operating System Installation
					</td>
				</tr>
				<tr data-tt-id="1-8" data-tt-parent-id="1">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Software Installation">Software Installation
					</td>
				</tr>
				<tr data-tt-id="1-9" data-tt-parent-id="1">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Tutorials">Tutorials
					</td>
				</tr>
				<tr data-tt-id="1-10" data-tt-parent-id="1">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Virus Removal">Virus Removal
					</td>
				</tr>
				<tr data-tt-id="1-11" data-tt-parent-id="1">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Wireless Networking">Wireless Networking
					</td>
				</tr>


				<tr data-tt-id="2">
					<td><a href="">Home Theater</a></td>
				</tr>
				<tr data-tt-id="2-1" data-tt-parent-id="2">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Remote Control Setup">Remote Control Setup
					</td>
				</tr>
				<tr data-tt-id="2-2" data-tt-parent-id="2">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Setup Surround System">Setup Surround System
					</td>
				</tr>
				<tr data-tt-id="2-3" data-tt-parent-id="2">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Home Theater Setup">Home Theater Setup
					</td>
				</tr>
				<tr data-tt-id="2-4" data-tt-parent-id="2">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Troubleshoot Surround System/Home Theater Setup">Troubleshoot Surround System/Home Theater Setup
					</td>
				</tr>

				<tr data-tt-id="3">
					<td><a href="">Mobile Setup</a></td>
				</tr>
				<tr data-tt-id="3-1" data-tt-parent-id="3">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Smartphone Setup and Tutorial ">Smartphone Setup and Tutorial
					</td>
				</tr>
				<tr data-tt-id="3-2" data-tt-parent-id="3">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Sync & Backup">Sync & Backup
					</td>
				</tr>
				<tr data-tt-id="3-3" data-tt-parent-id="3">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Setup E-mail">Setup E-mail
					</td>
				</tr>

				<tr data-tt-id="4">
					<td><a href="">Network Support</a></td>
				</tr>
				<tr data-tt-id="4-1" data-tt-parent-id="4">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="New Network Setup">New Network Setup
					</td>
				</tr>
				<tr data-tt-id="4-2" data-tt-parent-id="4">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Router Setup">Router Setup
					</td>
				</tr>
				<tr data-tt-id="4-3" data-tt-parent-id="4">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Router Troubleshoot and Repair ">Router Troubleshoot and Repair
					</td>
				</tr>
				<tr data-tt-id="4-4" data-tt-parent-id="4">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Wifi Extender">Wifi Extender
					</td>
				</tr>

				<tr data-tt-id="5">
					<td><a href="">Printer Serivces</a></td>
				</tr>
				<tr data-tt-id="5-1" data-tt-parent-id="5">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Printer Setup">Printer Setup
					</td>
				</tr>
				<tr data-tt-id="5-2" data-tt-parent-id="5">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Printer Troubleshoot">Printer Troubleshoot
					</td>
				</tr>
				<tr data-tt-id="5-3" data-tt-parent-id="5">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Ink/Cartridge Replacement">Ink/Cartridge Replacement
					</td>
				</tr>


				<tr data-tt-id="6">
					<td><a href="">Insurance</a></td>
				</tr>
				<tr data-tt-id="6-1" data-tt-parent-id="6">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="iPhone">iPhone/Any Smartphone/iPad Insurance
					</td>
				</tr>
				<tr data-tt-id="6-2" data-tt-parent-id="6">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="laptop">Laptop Insurance
					</td>
				</tr>
				<tr data-tt-id="6-3" data-tt-parent-id="6">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="camera">Camera Insurance
					</td>
				</tr>

				<tr data-tt-id="7">
					<td><a href="">Website Solutions</a></td>
				</tr>
				<tr data-tt-id="7-1" data-tt-parent-id="7">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Web Development">Web Development
					</td>
				</tr>
				<tr data-tt-id="7-2" data-tt-parent-id="7">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Logo Design">Logo Design
					</td>
				</tr>
				<tr data-tt-id="7-3" data-tt-parent-id="7">
					<td>
						<input style="width:20px" type="checkbox" name="service[]" value="Hosting Server w/Management">Hosting Server w/Management
					</td>
				</tr>
			</table>
		</div>
		<input type="button" name="previous" class="previous action-button" value="Previous" />
		<input type="button" name="next" class="next action-button" value="Next" />
	</fieldset>
	<fieldset>
	-<?php
		if(isset($user)){
			echo "
			<script type=\"text/javascript\">
					function MyAutoRun2(){
						document.getElementById('d1').remove();
						document.getElementById('d2').remove();
						document.getElementById('d3').remove();
						document.getElementById('d4').remove();
						document.getElementById('d5').remove();
						document.getElementById('d6').remove();
						document.getElementById('d7').remove();
						document.getElementById('d8').remove();
					}
					</script>

			";
	}
	?>
		<h2 class="fs-title">Additional Information</h2>
		<h3 class="fs-subtitle">last step</h3>
		<input type="text" name="fname" placeholder="First Name" id="d1"/>
		<input type="text" name="lname" placeholder="Last Name" id="d2"/>
		<input type="text" name="email" placeholder="Email" id="d3"/>
		<input type="text" name="phone" placeholder="Phone" id="d4"/>
		<input type="text" name="address" placeholder="Address Line" id="d5"/>
		<textarea name="additional" placeholder="Additional Information"></textarea>
		<input type="button" name="previous" class="previous action-button" value="Previous" />
		<input type="submit" name="submit" class="submit action-button" value="Submit" />
	</fieldset>
{!! Form::close() !!}



<div id="loginmodal" style="display:none;">
	{!! Form::open(['id'=>'loginform','url'=>'/customer/login']) !!}

		<h2 class="fs-title">User Login</h2>
		<input type="text" name="email" placeholder="Email" />
		<input type="password" name="password" placeholder="Password" />

		<input type="submit" name="submit" class="login action-button" value="Login" />


	{!! Form::close() !!}
</div>

<div id="registermodal" style="display:none;">
	{!! Form::open(['id'=>'registerform','url'=>'/customer/register']) !!}
		<h2 class="fs-title">User Register</h2>
		<input type="text" name="firstname" placeholder="First Name"/>
		<input type="text" name="lastname" placeholder="Last Name"/>
		<input type="text" name="email" placeholder="Email"/>
		<input type="text" name="phone" placeholder="Phone"/>
		<input type="text" name="address" placeholder="Address Line"/>
		<input type="password" name="password" placeholder="Password" />
		<input type="password" name="password_confirmation" placeholder="Retype Password" />

		<input type="submit" name="submit" class="register action-button" value="Register" />

	{!! Form::close() !!}
</div>



<script src="http://thecodeplayer.com/uploads/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
<script src="js/jquery.leanModal.min.js" type="text/javascript"></script>
<script src="js/service.js" type="text/javascript"></script>
<script type="text/javascript">
	function logout(){
		window.location = "./customer/logout";
	}
	$(function(){
		$('#modaltrigger').leanModal({
			top:110,
			overlay:0.45,
			closeButton:".hidemodal"
		});

		$('#modaltrigger2').leanModal({
			top:110,
			overlay:0.45,
			closeButton:".hidemodal"
		});

	});
</script>

<link href="css/jquery.treetable.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.treetable.js"></script>
<script>
	$("#services").treetable({ 	expandable: true,
								clickableNodeNames : true
	});

</script>

<br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br>
<div style="text-align:center;clear:both">
</div>
</body>
</html>