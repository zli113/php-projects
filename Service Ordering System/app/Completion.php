<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Completion extends Model
{
    protected $fillable = ['service_id', 'technician_id'];
}
