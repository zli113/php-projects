<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Technician extends Model
{
    protected $fillable = ['firstname', 'lastname', 'email', 'password'];
}
