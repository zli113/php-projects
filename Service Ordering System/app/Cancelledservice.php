<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancelledservice extends Model
{
    protected $fillable = ['service_id', 'reason'];
}
