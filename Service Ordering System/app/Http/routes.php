<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*use App\Role;*/

Route::get('/', function(){
//    $customer=Role::create(['name'=>'customer']);
//    $support=Role::create(['name'=>'support']);
//    $technician=Role::create(['name'=>'technician']);
    return view('index');
});

//Route::get('/', function () {
//    return view('pages.login');
//});

//customer route

Route::get('/customer', array('as' => 'index','uses' => 'CustomerController@index'));

Route::post('/customer/login', 'Auth\AuthController@postLogin');

Route::post('/customer/register', 'Auth\AuthController@postRegister');

Route::get('/customer/logout', 'Auth\AuthController@getLogout');

Route::post('/customer/services', 'CustomerController@store');

//support team route

Route::get('/support','SupportController@index');

Route::post('/support/login','SupportController@login');

Route::get('/support/profile','SupportController@showprofile');

Route::post('/support/updateProfile','SupportController@updateProfile');

Route::get('/support/logout','SupportController@logout');

Route::get('/support/home','SupportController@home');

Route::get('/support/completed','SupportController@showCompleted');

Route::get('/support/unscheduled','SupportController@showUnscheduled');

Route::get('/support/searchByName','SupportController@searchByName');

Route::get('/support/searchByServiceID','SupportController@searchByServiceID');

Route::post('/support/getService','SupportController@getService');

Route::get('/support/listCustomer','SupportController@listCustomer');

Route::get('/support/editService','SupportController@editService');

Route::get('/support/edit/{id}','SupportController@edit');

Route::post('/support/edit','SupportController@editSave');

Route::get('/support/addService','SupportController@addService');

Route::post('/support/add','SupportController@addSave');

//technician team route

Route::get('/technician','TechnicianController@index');

Route::post('/technician/login','TechnicianController@login');

Route::get('/technician/home','TechnicianController@home');

Route::get('/technician/completedServices','TechnicianController@showCompletedServices');

Route::get('/technician/todayServices','TechnicianController@showTodayServices');

Route::get('/technician/addService','TechnicianController@addService');

Route::post('/technician/add','TechnicianController@addSave');

Route::get('/technician/editService','TechnicianController@showAssignedServices');

Route::get('/technician/edit/{id}','TechnicianController@edit');

Route::post('/technician/editSave','TechnicianController@editSave');

Route::get('/technician/profile','TechnicianController@showprofile');

Route::post('/technician/updateProfile','TechnicianController@updateProfile');

Route::get('/technician/logout','TechnicianController@logout');

