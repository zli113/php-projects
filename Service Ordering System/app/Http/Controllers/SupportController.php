<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use DateTimeZone;

use DB;
use Session;
use App\User;
use App\Service;
use App\Support;
use App\Assignment;
use App\Completion;

use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Controllers\Controller;

class SupportController extends Controller
{
    public function index(){
        //Session::flush();
        return view('support.login');
    }

    public function login(request $request){
        $input=$request->all();

        $user = DB::table('supports')->where('email', $input['email'])->first();

        if(empty($user))
            return redirect('/support')->with('status', 'Email Address Not Found');

        if($input['password']==$user->password){
            Session::put('id', $user->id);
            Session::put('firstname', $user->firstname);
            Session::put('lastname', $user->lastname);
            Session::put('email', $user->email);
            return redirect('/support/home');
        }

        else{
            return redirect('/support')->with('status', 'Password is Wrong');
        }

    }

    public function showprofile(){
        if(Session::has('email')){
            return view('support.profile');
        }

        else{
            return redirect('/support');
        }
    }

    public function updateProfile(request $request){
        if(Session::has('email')){
            $input=$request->all();

            $support=Support::find(Session::get('id'));
            $support['firstname']=$input['firstname'];
            $support['lastname']=$input['lastname'];
            $support['email']=$input['email'];

            $support->save();

            Session::put('firstname', $input['firstname']);
            Session::put('lastname', $input['lastname']);
            Session::put('email', $input['email']);

            $status="Profile updated";
            return view('support.profile', compact('status'));
        }

        else{
            return redirect('/support');
        }
    }

    public function logout(){
        Session::flush();

        return redirect('/support/home');
    }

    public function home(){

        if(Session::has('email')){

            $services = DB::table('services')->where('status', 'scheduled/uncompleted')->get();

            return view('support.index',compact('services'));

        }

        else{
            return redirect('/support');
        }
    }

    public function showCompleted(){
        if(Session::has('email')){

            $services = DB::table('services')
                ->join('completions', 'services.id', '=', 'completions.service_id')
                ->where('services.status', 'completed')
                ->select('services.*', 'completions.*')
                ->get();

            return view('support.completedServices',compact('services'));
        }

        else{
            return redirect('/support');
        }
    }


    public function showUnscheduled(){
        if(Session::has('email')){

            $services = DB::table('services')->where('status', 'unscheduled')->get();

            return view('support.unscheduledServices',compact('services'));
        }
        else{
            return redirect('/support');
        }
    }

    public function searchByName(){
        if(Session::has('email')){

            $services = DB::table('services')->get();

            return view('support.searchByName',compact('services'));

        }
        else{
            return redirect('/support');
        }
    }

    public function searchByServiceID(){
        if(Session::has('email')){

            return view('support.searchByServiceID');

        }
        else{
            return redirect('/support');
        }
    }

    public function getService(request $request){
        if(Session::has('email')) {
            $input = $request->all();

            $service = DB::table('services')->where('id', $input['service_id'])->first();

            return view('support.searchByServiceID', compact('service'));
        }
        else{
            return redirect('/support');
        }
    }

    public function listCustomer(){
        if(Session::has('email')){

            $users = DB::table('users')->get();

            return view('support.listCustomer', compact('users'));
        }
        else{
            return redirect('/support');
        }
    }

    public function editService(){
        if(Session::has('email')){

            $services = DB::table('services')->get();

            return view('support.editService',compact('services'));
        }
        else{
            return redirect('/support');
        }
    }

    public function edit($id){
        if(Session::has('email')){

            $service=Service::find($id);

            return view('support.editAService', compact('service'));
        }
        else{
            return redirect('/support');
        }
    }

    public function editSave(request $request){
        if(Session::has('email')){
            $input=$request->all();

            $service=Service::find($input['service_id']);
            $tag=null;
            if($service['status']=="unscheduled" and $input['technician_id'] and $input['assigned_date']){
                $assignment=new Assignment;
                $assignment['service_id']=$input['service_id'];
                $assignment['technician_id']=$input['technician_id'];
                $assignment['assigned_time']=$input['assigned_date'];
                $tag="scheduled/uncompleted";
                $assignment->save();
            }

            if($service['status']=="scheduled/uncompleted" and  $input['status']=="unscheduled"){
                DB::table('assignments')->where('service_id',$input['service_id'])->delete();
            }

            if($service['status']=="scheduled/uncompleted" and  $input['status']=="completed"){
                $assigned=DB::table('assignments')->where('service_id',$input['service_id'])->get();
                foreach($assigned as $ass){
                    $complete = new Completion;
                    $complete['service_id']=$input['service_id'];
                    $complete['technician_id']=$ass->technician_id;
                    $complete['assigned_time']=$ass->assigned_time;
                    $complete->save();
                }
                DB::table('assignments')->where('service_id',$input['service_id'])->delete();

            }

            $service['service_name']=$input['service_name'];
            $service['additional_information']=$input['additional_information'];
            if($tag=="scheduled/uncompleted")
                $service['status']=$tag;
            else
                $service['status']=$input['status'];

            $service->save();

            return redirect('/support/editService')->with('status', 'Service updated!');
        }
        else{
            return redirect('/support');
        }
    }

    public function addService(){

        if(Session::has('email')){

            return view('support.addNewService');
        }
        else{
            return redirect('/support');
        }
    }

    public function addSave(request $request){
        if(Session::has('email')){
            $input=$request->all();

            $user=User::find($input['user_id']);
            $user_name=$user->firstname." ".$user->lastname;
            $service = new Service;
            $service['service_name']=$input['service_name'];
            $service['user_id']=$input['user_id'];
            $service['user_name']=$user_name;
            $service['additional_information']=$input['additional_information'];
            $service['status']="unscheduled";
            $service['submit_time'] = Carbon::now(new DateTimeZone('America/Chicago'));

            $service->save();

            return redirect('/support/editService')->with('status', 'Service added!');
        }
        else{
            return redirect('/support');
        }
    }

}
