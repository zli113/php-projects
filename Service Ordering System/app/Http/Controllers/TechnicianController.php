<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Session;
use App\User;
use App\Service;
use App\Technician;
use App\Completion;
use App\Cancelledservice;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TechnicianController extends Controller
{
    public function index(){
        //Session::flush();
        return view('technician.login');
    }

    public function login(request $request){
        $input=$request->all();

        $user = DB::table('technicians')->where('email', $input['email'])->first();

        if(empty($user))
            return redirect('/technician')->with('status', 'Email Address Not Found');

        if($input['password']==$user->password){
            Session::put('id', $user->id);
            Session::put('firstname', $user->firstname);
            Session::put('lastname', $user->lastname);
            Session::put('email', $user->email);
            return redirect('/technician/home');
        }

        else{
            return redirect('/technician')->with('status', 'Password is Wrong');
        }
    }

    public function home(){
        if(Session::has('email')){

            $services = DB::table('services')
                ->join('assignments', 'services.id', '=', 'assignments.service_id')
                ->where('assignments.technician_id', Session::get('id'))
                ->select('services.*', 'assignments.assigned_time')
                ->get();

            return view('technician.index', compact('services'));
        }
        else{
            return redirect('/technician');
        }
    }

    public function showCompletedServices(){
        if(Session::has('email')){

            $services = DB::table('services')
                ->join('completions', 'services.id', '=', 'completions.service_id')
                ->where('completions.technician_id', Session::get('id'))
                ->select('services.*', 'completions.*')
                ->get();

            return view('technician.completedServices', compact('services'));
        }
        else{
            return redirect('/technician');
        }
    }

    public function showTodayServices(){
        if(Session::has('email')){
            $services = DB::table('assignments')->where('technician_id',Session::get('id'))->get();

            date_default_timezone_set("America/Chicago");
            $data1=date("Y-m-d 00:00:00");
            $data2=date("Y-m-d 23:59:59");
            $services = DB::table('services')
                ->join('assignments', 'services.id', '=', 'assignments.service_id')
                ->where('assignments.technician_id', Session::get('id'))
                ->where('assignments.assigned_time','>',$data1)
                ->where('assignments.assigned_time','<',$data2)
                ->select('services.*', 'assignments.assigned_time')
                ->get();

            return view('technician.todayServices', compact('services'));
        }
        else{
            return redirect('/technician');
        }
    }

    public function addService(){
        if(Session::has('email')){
            return view('technician.addNewService');
        }
        else{
            return redirect('/technician');
        }
    }

    public function addSave(request $request){
        if(Session::has('email')){
            $input=$request->all();

            $user=DB::table('users')->find($input['user_id']);
            $user_name=$user->firstname." ".$user->lastname;

            $service = new Service;
            $service['service_name']=$input['service_name'];
            $service['user_id']=$input['user_id'];
            $service['user_name']=$user_name;
            $service['additional_information']=$input['additional_information'];
            $service['status']="completed";
            $service->save();

            $complete=new Completion;
            $complete['service_id']=$service['id'];
            $complete['technician_id']=$input['technician_id'];
            $complete['assigned_time']=$input['assigned_date'];
            $complete->save();

            return redirect('/technician/completedServices');
        }
        else{
            return redirect('/technician');
        }
    }

    public function showAssignedServices(){
        if(Session::has('email')){

            $services = DB::table('services')
                ->join('assignments', 'services.id', '=', 'assignments.service_id')
                ->where('assignments.technician_id', Session::get('id'))
                ->select('services.*', 'assignments.assigned_time')
                ->get();

            return view('technician.editService', compact('services'));
        }
        else{
            return redirect('/technician');
        }
    }

    public function edit($id){
        if(Session::has('email')){
            $service=Service::find($id);
            return view('technician.editAService', compact('service'));
        }
        else{
            return redirect('/technician');
        }
    }

    public function editSave(request $request){
        if(Session::has('email')){
            $input=$request->all();
            $service=Service::find($input['service_id']);
            if($input['action']=="unschedule"){
                $service['status']="unshceduled";
                $service->save();
                DB::table('assignments')->where('service_id',$input['service_id'])->delete();
            }

            if($input['action']=="cancel"){
                $cancel=new Cancelledservice;
                $cancel['service_id']=$input['service_id'];
                $cancel['reason']=$input['reason'];
                $cancel->save();
                DB::table('assignments')->where('service_id',$input['service_id'])->delete();
                DB::table('services')->where('id',$input['service_id'])->delete();
            }

            if($input['action']=="complete"){
                $service['status']="completed";
                $service->save();

                $assigned=DB::table('assignments')->where('service_id',$input['service_id'])->get();
                foreach($assigned as $ass){
                    $complete = new Completion;
                    $complete['service_id']=$input['service_id'];
                    $complete['technician_id']=$ass->technician_id;
                    $complete['assigned_time']=$ass->assigned_time;
                    $complete->save();
                }
                DB::table('assignments')->where('service_id',$input['service_id'])->delete();
            }

            return redirect('/technician/editService')->with('status', 'Service updated!');
        }
        else{
            return redirect('/technician');
        }
    }

    public function showprofile(){
        if(Session::has('email')){
            return view('technician.profile');
        }

        else{
            return redirect('/technician');
        }
    }

    public function updateProfile(request $request){
        if(Session::has('email')){
            $input=$request->all();

            $technician=Technician::find(Session::get('id'));
            $technician['firstname']=$input['firstname'];
            $technician['lastname']=$input['lastname'];
            $technician['email']=$input['email'];

            $technician->save();

            Session::put('firstname', $input['firstname']);
            Session::put('lastname', $input['lastname']);
            Session::put('email', $input['email']);

            $status="Profile updated";
            return view('technician.profile', compact('status'));
        }

        else{
            return redirect('/techniciant');
        }
    }

    public function logout(){
        Session::flush();
        return redirect('/technician/home');
    }
}
