<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use DateTimeZone;

use App\User;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    //
    public function index(){
        $user = \Auth::user();

        //return $user;
        return view('customer.addService', compact('user'));
    }

    public function store(request $request){
        $user = \Auth::user();
        $input=$request->all();

        foreach($input['service'] as $ser){
            $Service = new Service;

            $Service['service_name'] = $ser;
            $Service['user_id'] = $user['id'];
            $Service['user_name'] = $user['firstname']." ".$user['lastname'];
            $Service['additional_information'] = $input['additional'];
            $Service['status']="unscheduled";
            $Service['submit_time'] = Carbon::now(new DateTimeZone('America/Chicago'));

            $Service->save();
        }

        return redirect('customer')->with('status', 'Services Added!');

    }

}
