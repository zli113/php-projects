<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>

<html>
  <head>
  
    <title>开始选题</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Web site" />
    <meta name="description" content="Site description here" />
    <meta name="keywords" content="keywords here" />
    <meta name="language" content="en" />
    <meta name="subject" content="Site subject here" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Your company" />
    <meta name="abstract" content="Site description here" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <link id="theme" rel="stylesheet" type="text/css" href="style.css" title="theme" />
  <script language=JavaScript>
		function logout(){
		if (confirm("您确定要退出吗？"))
		top.location = "logout";
		return false;
		}
	</script>	
  </head>
  <link href="__PUBLIC__/css/style.css" rel="stylesheet" type="text/css">
   <link href="__PUBLIC__/css/buttons.css" rel="stylesheet" type="text/css">
  <body> 
    <div id="wrapper"> 
      <div id="bg"> 
        <div id="header"></div>  
        <div id="page"> 
          <div id="container"> 
            <!-- banner -->  
             <div id="banner">
			<center><span class="left_b">教师<u><a href="">	<?php echo ($_SESSION['teachername']); ?></a></u> 您好,感谢登陆使用</span>
			<a href="#" target="_self" onClick="logout();"><input type="button" class="button gray medium" value="安全退出"></a></center>
			</div>  
            <!-- end banner -->  
            <!-- horizontal navigation -->  
            <div id="nav1"> 
              <ul>
                <li>
                  <a href="__URL__/home" shape="rect">首页</a>
                </li>
                <li >
                  <a href="__URL__/info" shape="rect">个人信息</a>
				  <ul> 
						<li><a href="__URL__/info">个人信息</a></li> 
						<li><a href="__URL__/change">信息修改</a></li> 
				  </ul> 
                </li>
                <li   style="border:none;background:transparent url('images/css/hmenu-sel.jpg') repeat-x top left;">
                  <a href="__URL__/begin" shape="rect">命题</a>
				  <ul> 
						<li><a href="__URL__/begin">开始命题</a></li> 
						<li><a href="__URL__/havecreated">已命题</a></li> 
						<li><a href="__URL__/decide">选题决定</a></li> 
						<li><a href="__URL__/result">选题结果</a></li> 
				  </ul> 
                </li>
                <li>
                  <a href="__URL__/mid_report" shape="rect">报告评论</a>
				  <ul> 
						<li><a href="__URL__/mid_report">中期报告</a></li>  
						<li><a href="__URL__/view_comment">查看评论</a></li> 
				  </ul> 
                </li>
                <li>
                  <a href="__URL__/download_report" shape="rect">下载论文</a>
                </li>
              </ul> 
            </div>  
            <!-- end horizontal navigation -->  
            <!--  content -->  
            <div id="content"> 
              <div id="centernew"> 
                <div id="welcome"> 
                  <h1 align=center>请开始您的课程设计命题</h1>
                </div> 
				<form action="__URL__/creat_subject" method="POST">
				<table align=center style="padding:20px">
						<tr>
						<td style="padding:10px"><h3>学院</h3></td>
						<td><?php echo ($_SESSION['teachercoll']); ?>
						</td>
					</tr>
					<tr>
						<td style="padding:10px"><h3>专业方向</h3></td>
						<td><?php echo ($_SESSION['teacherori']); ?>
						</td>
					</tr>
					<tr><td style="padding:10px"><h3>命题名称</h3></td><td style="padding:0 0 0 3px"><input type="text" name="title" class="new1"></textarea></td><tr>
					<tr><td style="padding:10px"><h3>题目介绍</h3></td><td><label for="comment"></label><textarea name="info" id="comment" cols="50" rows="10"></textarea></td><tr>
					<tr><td colspan="3" align=center style="padding:10px"><input type="submit" id="submit" value="发布"></td>
					</table>
				</form>	
              </div>  
              <div id="right"> 
                
              </div>  
              <div class="clear" style="height:40px"></div> 
            </div>  
            <!-- end content --> 
          </div>  
          <!-- end container --> 
        </div>  
        <div id="footerWrapper"> 
          <div id="footer"> 
            <p style="padding-top:10px"> 
                
              Copyright 2013
            </p> 
          </div> 
        </div> 
      </div> 
    </div> 
  </body>
</html>