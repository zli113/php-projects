<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>

<html>
  <head>
    <title>西电课程设计选题系统</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Web site" />
    <meta name="description" content="Site description here" />
    <meta name="keywords" content="keywords here" />
    <meta name="language" content="en" />
    <meta name="subject" content="Site subject here" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Your company" />
    <meta name="abstract" content="Site description here" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
  </head>
  <link href="__PUBLIC__/css/style.css" rel="stylesheet" type="text/css">
  <link href="__PUBLIC__/css/buttons.css" rel="stylesheet" type="text/css">
  <body> 
    <div id="wrapper"> 
      <div id="bg"> 
        <div id="header"></div>  
        <div id="page"> 
          <div id="container"> 
            <!-- banner -->  
            <div id="banner"></div>  
            <!-- end banner -->  
            <!-- horizontal navigation -->  
            <div id="nav1"> 
              
            </div>  
            <!-- end horizontal navigation -->  
            <!--  content -->  
            <div id="content"> 
              <div id="center"> 
                <div id="welcome"> 
                  <h2>通知公告</h2>  
                  <p>
				  ...<br>...<br>...<br>
                  </p>  
                  <h2>注意事项</h1>  
                  <p>
				  ...<br>...<br>...<br>
				  </p>  
                  
                  <h2>相关文档</h2>  
                  <p>
				  ...<br>...<br>...<br>
				  </p>  
                  <h3>其他</h3>  
                  <p>
				  ...<br>...<br>...<br>
				  </p>  
                  <p style="clear:both" /> 
                </div> 
              </div>  
              <div id="right"> 
                <div id="sidebar"> 
                  <h2>用户登录</h2> 
				  <br>	
			<form action="__URL__/checkUser" method="post">
                  <table>
					<tr>
						<td><h3>账号</h3></td>
						<td><input type="text" name="number" onClick="this.value='';" onFocus="this.select()" onBlur="this.value=!this.value?'请输入学号':this.value;" value="请输入学号"></td>
					</tr>
					<tr></tr><tr></tr><tr></tr><tr></tr>
					<tr>
						<td><h3>密码</h3></td>
						<td><input type="password" name="password"></td>
					</tr>
					<tr></tr><tr></tr><tr></tr><tr></tr>
					<tr>
						<td><h3>类型</h3></td>
						<td style="padding:0 0 0 3px"><select name='select'>
							<option>学生</option>
							<option>教师</option>
							<option>管理员</option>
							</select>
						</td>
					</tr>
				  </table>
                  <input type="submit" class="button small blue" value="登录">
				  <a href="__URL__/reg"><input type="button" class="button small orange" value="注册"></a><br>
			</form>
                  <div style="font-weight:bold;margin-top:70px">Call us : 18109230129</div>  
                  <div style="text-align:center;margin:20px 0"> 
                    <img src="images/call-1.jpg" /> 
                  </div> 
                </div> 
              </div>  
              <div class="clear" style="height:40px"></div> 
            </div>  
            <!-- end content --> 
          </div>  
          <!-- end container --> 
        </div>  
        <div id="footerWrapper"> 
          <div id="footer"> 
            <p style="padding-top:10px"> 
                
              Copyright 2013
            </p> 
          </div> 
        </div> 
      </div> 
    </div> 
  </body>
</html>