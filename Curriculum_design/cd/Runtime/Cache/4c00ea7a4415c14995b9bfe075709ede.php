<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>

<html>
  <head>
  
    <title>个人信息</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Web site" />
    <meta name="description" content="Site description here" />
    <meta name="keywords" content="keywords here" />
    <meta name="language" content="en" />
    <meta name="subject" content="Site subject here" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Your company" />
    <meta name="abstract" content="Site description here" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <link id="theme" rel="stylesheet" type="text/css" href="style.css" title="theme" />
	<script language=JavaScript>
		function logout(){
		if (confirm("您确定要退出吗？"))
		top.location = "logout";
		return false;
		}
	</script>	
  </head>
  <link href="__PUBLIC__/css/style.css" rel="stylesheet" type="text/css">
   <link href="__PUBLIC__/css/buttons.css" rel="stylesheet" type="text/css">
  <body> 
    <div id="wrapper"> 
      <div id="bg"> 
        <div id="header"></div>  
        <div id="page"> 
          <div id="container"> 
            <!-- banner -->  
           <div id="banner">
			<center><span class="left_b">学生<u><a href="">	<?php echo ($_SESSION['headname']); ?></a></u> 您好,感谢登陆使用</span>
			<a href="#" target="_self" onClick="logout();"><input type="button" class="button gray medium" value="安全退出"></a></center>
			</div>  
            <!-- end banner -->  
            <!-- horizontal navigation -->  
            <div id="nav1"> 
              <ul>
                <li  >
                  <a href="__URL__/home" shape="rect">首页</a>
                </li>
                <li style="border:none;background:transparent url('images/css/hmenu-sel.jpg') repeat-x top left;">
                  <a href="__URL__/info" shape="rect">个人信息</a>
				  <ul> 
						<li><a href="__URL__/info">个人信息</a></li> 
						<li><a href="__URL__/member_add">成员添加</a></li> 
						<li><a href="__URL__/member">成员信息</a></li>
						<li><a href="__URL__/change">信息修改</a></li> 
				  </ul> 
                </li>
                <li>
                  <a href="__URL__begin" shape="rect">开始选题</a>
				  <ul> 
						<li><a href="__URL__/begin">开始选题</a></li> 
						<li><a href="__URL__/creat">自命题</a></li> 
						<li><a href="__URL__/result">查看结果</a></li> 
				  </ul> 
                </li>
                <li>
                  <a href="__URL__/mid_report" shape="rect">报告评论</a>
				  <ul> 
						<li><a href="__URL__/mid_report">中期报告</a></li>  
						<li><a href="__URL__/view_comment">查看评论</a></li> 
				  </ul> 
                </li>
                <li>
                  <a href="__URL__begin/submit_report" shape="rect">上传论文</a>
                </li>
              </ul> 
            </div> 
            <!-- end horizontal navigation -->  
            <!--  content -->  
            <div id="content"> 
              <div id="centernew"> 
                <div id="welcome"> 
                  <h1 align=center>您的个人信息</h1>
                </div> 
				<table align=center style="padding:50px">
				<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): ++$i;$mod = ($i % 2 )?><tr><td><h3>学号：</h3></td><td width=50px></td><td><?php echo ($vo["headnumber"]); ?></td></tr>
						<tr><td><h3>姓名：</h3></td><td width=50px></td><td><?php echo ($vo["headname"]); ?></td></tr>
						<tr><td><h3>学院：</h3></td><td width=50px></td><td><?php echo ($vo["headcollege"]); ?></td></tr>
						<tr><td><h3>专业方向：</h3></td><td width=50px></td><td><?php echo ($vo["headorien"]); ?></td></tr>
						<tr><td><h3>联系方式：</h3></td><td width=50px></td><td><?php echo ($vo["headphone"]); ?></td></tr>
						<tr><td><h3>电子邮箱：</h3></td><td width=50px></td><td><?php echo ($vo["headmail"]); ?></td></tr><?php endforeach; endif; else: echo "" ;endif; ?>		
						</tr>
					</table>
              </div>  
              
              <div class="clear" style="height:40px"></div> 
            </div>  
            <!-- end content --> 
          </div>  
          <!-- end container --> 
        </div>  
        <div id="footerWrapper"> 
          <div id="footer"> 
            <p style="padding-top:10px"> 
                
              Copyright 2013
            </p> 
          </div> 
        </div> 
      </div> 
    </div> 
  </body>
</html>