<?php
// 本文档自动生成，仅供测试运行
class IndexAction extends Action
{
    /**
    +----------------------------------------------------------
    * 默认操作
    +----------------------------------------------------------
    */
    function _initialize()
	{	
		 header('Content-Type:text/html; charset=utf-8');//防止出现乱码
		 if($_SESSION['type'])
		 {
		//	unset($_SESSION[C('USER_AUTH_KEY')]);
		//	unset($_SESSION);
		//	session_destroy();
		 }

	}
	
	//注册
	public function insert()
	{
		//session_start();
		$_SESSION['login']=false;
		$DB=D('headman');
		$hid=$_POST['hnumber'];
		$name=$_POST['hname'];
		$passwd1=$_POST['hpassword1'];
		$passwd2=$_POST['hpassword2'];
		$college=$_POST['hcollege'];
		$orien=$_POST['horien'];
		$phone=$_POST['hphone'];
		$mail=$_POST['hmail'];
		$sregip=getenv(REMOTE_ADDR);
		$hregtime=date("Y-m-d H:i:s");
		if(!$passwd1)
		{
		echo "<script language=\"JavaScript\">alert(\"密码不能为空\");self.location='reg';</script>"; 
		//$this->redirect('profile');
		}
		if($passwd1!=$passwd2)
		{
		echo "<script language=\"JavaScript\">alert(\"两次密码不同，请重新输入\");self.location='reg';</script>"; 
		}
		$map=array();
		$map['headnumber']=$hid;
		$list=$DB->where($map)->find();
		if($list)
		{
		echo "<script language=\"JavaScript\">alert(\"这个学号已经注册\");self.location='reg';</script>"; 
		//$this->redirect('reg');
		}
		else
		{
		$map1['headnumber']=$hid;
		$map1['headname']=$name;
		$map1['hpassword']=$passwd1;
		$map1['headcollege']=$college;
		$map1['headorien']=$orien;
		$map1['headphone']=$phone;
		$map1['headmail']=$mail;
		$map1['hregtime']=date("Y-m-d H:i:s");
		//dump($map1);
		$result=$DB->add($map1);
		if($result>0){
		$this->assign("jumpUrl",__URL__.'/index');
		$this->success('注册成功');}
		}

	}
//验证用户名密码开始
	public function checkUser()
	{
		session_start();
		$select=$_POST['select'];

		if($select==='学生'){
					    $_SESSION['login']=false;
					    $DB=D('headman');
						$usernum=$_POST['number'];
						$pwd=$_POST['password'];
						 if (empty($usernum))
						{
						  echo "input your name:";
						  $this->redirect('Index/','',0,'');
						}
						 else if (empty($pwd))
						{
						 echo "input yor passwd:";
						  $this->redirect('Index/','',0,'');
						}
					//     else $this->redirect('Index/info','',0,'');
						 $map=array(); 
						 $map['headnumber']=$usernum;
						 $map['hpassword']=$pwd;
						 $list=$DB->where($map)->find();
							if(!$list)   
						  {
							 echo "<script language=\"JavaScript\">alert(\"用户名或密码错误\");self.location='index';</script>"; 
							 //$this->redirect('Index/','',0,''); 
						  } 
							else    
						   {
							//echo $list['numble'];
						   $_SESSION['hgroupnumber']=$list['hgroupnumber'];
						   $_SESSION['headnumber']=$list['headnumber'];
							 // echo $_SESSION['numble'];	
						   $_SESSION['hpassword']=$list['hpassword'];
						   //$_SESSION['SCOLLEGE']=$list['SCOLLEGE'];
						   $_SESSION['headname']=$list['headname'];
						   $_SESSION['headcollege']=$list['headcollege'];
						   $_SESSION['headorien']=$list['headorien'];
						   $_SESSION['headphone']=$list['headphone'];
						   $_SESSION['headmail']=$list['headmail'];
						   $_SESSION['hregtime']=$list['hregtime'];
						   $_SESSION['login']=true;
						   $_SESSION[C('USER_AUTH_KEY')]=$list['headname'];
						   $_SESSION['KIND']="同学";
						   $this->assign("jumpUrl",__URL__.'/home');
						   $this->success('success!');
						   }
						   
		}
		if($select==='教师')
		{
						$_SESSION['login']=false;
					    $DB=D('teacher');
						$usernum=$_POST['number'];
						$pwd=$_POST['password'];
						 if (empty($usernum))
						{
						  echo "input your name:";
						  $this->redirect('Index/','',0,'');
						}
						 else if (empty($pwd))
						{
						 echo "input yor passwd:";
						  $this->redirect('Index/','',0,'');
						}
					//     else $this->redirect('Index/info','',0,'');
						 $map=array(); 
						 $map['teacherid']=$usernum;
						 $map['teacherpwd']=$pwd;
						 $list=$DB->where($map)->find();
							if(!$list)   
						  {
							 echo "<script language=\"JavaScript\">alert(\"用户名或密码错误\");self.location='index';</script>"; 
							 //$this->redirect('Index/','',0,''); 
						  } 
							else    
						   {
						   $_SESSION['teacherid']=$list['teacherid'];
						   $_SESSION['teachername']=$list['teachername'];
						   $_SESSION['teacherpwd']=$list['teacherpwd'];
						   $_SESSION['teachercoll']=$list['teachercoll'];
						   $_SESSION['teacherori']=$list['teacherori'];
						   $_SESSION['teacherphone']=$list['teacherphone'];
						   $_SESSION['teachermail']=$list['teachermail'];
						   $_SESSION['tregtime']=$list['tregtime'];
						   $_SESSION['login']=true;
						   $_SESSION[C('USER_AUTH_KEY')]=$list['teachername'];
						   $_SESSION['KIND']="教师";
						   $this->assign("jumpUrl",'__APP__/Back/home');
						   $this->success('success!');
						   }
		}
	}
//验证结束
//退出
	public function logout ()
	{
			if(isset($_SESSION[C('USER_AUTH_KEY')])) 
			{
			unset($_SESSION[C('USER_AUTH_KEY')]);
			unset($_SESSION);
			session_destroy();
	 // $this->assign("jumpUrl",__URL__.'/index');
	//  $this->success('success out');
			$this->redirect('index',array('cate_id'=>2),0,'byebye');
	//$this->display();

			}
			 else {echo "wrong"; $this->redirect('Index/','',0,'');}
	}
//显示个人信息
	public function info ()
	{
			 session_start();
			 $DB=M('headman');
			 $condition['headnumber']=$_SESSION['headnumber'];
			 $list=$DB->where($condition)->select();
			 //dump($list);
			 $this->assign('list',$list);
			 $this->display();
	 }
//成员添加
	public function add_member()
	{
			session_start();
			$DB=D('member');
			$member['mgroupnumber']=$_SESSION['hgroupnumber'];
			$result=$DB->where($member)->find();
			if($result['member1id']&&$result['member2id'])
				echo "<script language=\"JavaScript\">alert(\"小组成员已满，不能继续添加\");self.location='member';</script>"; 
			
			$m1id=$_POST['member1id'];
			$m1name=$_POST['member1name'];
			$m1phone=$_POST['member1phone'];
			$m1mail=$_POST['member1mail'];
			$m2id=$_POST['member2id'];
			$m2name=$_POST['member2name'];
			$m2phone=$_POST['member2phone'];
			$m2mail=$_POST['member2mail'];
			$map1=array();
			$map1['member1id']=$m1id;
			$list1=$DB->where($map1)->find();
			if($list1)
			{
				echo "<script language=\"JavaScript\">alert(\"第一个小组成员已经注册\");self.location='member';</script>"; 
				//$this->redirect('member_add');
			}
			$map2=array();
			$map2['member2id']=$m2id;
			$list2=$DB->where($map2)->find();
			if($list2)
			{
				echo "<script language=\"JavaScript\">alert(\"第二个小组成员已经注册\");self.location='member';</script>"; 
				//$this->redirect('member_add');
			}
				$map['mgroupnumber']=$_SESSION['hgroupnumber'];
				$map['member1id']=$m1id;
				$map['member1name']=$m1name;
				$map['member1phone']=$m1phone;
				$map['member1mail']=$m1mail;
				$map['member2id']=$m2id;
				$map['member2name']=$m2name;
				$map['member2phone']=$m2phone;
				$map['member2mail']=$m2mail;
				$result=$DB->add($map);
				//dump($map);
			if($result>0){
			$this->assign("jumpUrl",__URL__.'/info');
			$this->success('添加成功');}
	}
//显示成员信息
	public function member()
	{
			session_start();
			$DB=M('member');
			$condition['mgroupnumber']=$_SESSION['hgroupnumber'];
			$list=$DB->where($condition)->select();
			$this->assign('list',$list);
			$this->display();
	}
//修改个人信息
	public function changeinformation()
	{
			session_start();
			$DB=M('headman');
			$p=$_POST['headphone'];
			$q=$_POST['headmail'];
			if(!$p)
				$data['headphone']=$_SESSION['headphone'];
			else{
				$data['headphone']=$p;
				$_SESSION['headphone']=$p;}
			if(!$q)
				$data['headmail']=$_SESSION['headmail'];
			else{
				$data['headmail']=$q;
				$_SESSION['headmail']=$q;}
			$data['hgroupnumber']=$_SESSION['hgroupnumber'];
			$result=$DB->save($data);
					if($result!==false){
						$this->assign("jumpUrl",__URL__.'/info');
						$this->success('修改成功');
						}
	}
	
	public function creat()
	{
			session_start();
			$DB=M('teacher');
			$condition['teachercoll']=$_SESSION['headcollege'];
			$condition['teacherori']=$_SESSION['headorien'];
			$list=$DB->where($condition)->select();
			//dump($list);
			if($list)
				{
				$this->assign('list',$list);
				$this->display();
				}
	}
	
	public function creat_subject()
	{
			session_start();
			$DB=M('subject');
			$DB2=M('choose');
			$DB3=M('teacher');
			$teacher=$_POST['teacher'];
			$title=$_POST['title'];
			$info=$_POST['info'];
			$condition['cgroupid']=$_SESSION['hgroupnumber'];
			$map=$DB2->where($condition)->select();
			if($map)
			{
			echo "<script language=\"JavaScript\">alert(\"已经选题或者自命题，请查看选题结果\");self.location='result';</script>"; 
			//$this->assign("jumpUrl",__URL__.'/creat');
			}
			else{
			$condition1['teachername']=$teacher;
			$list1=$DB3->where($condition1)->find();
			//dump($list1);
			$map1['subteacherid']=$list1['teacherid'];
			$map1['substuid']=$_SESSION['headnumber'];
			$map1['subteachername']=$teacher;
			$map1['subtitle']=$title;
			$map1['subinfo']=$info;
			$map1['subcoll']=$_SESSION['headcollege'];
			$map1['suborien']=$_SESSION['headorien'];
			$map1['subsubtype']="自命题";
			$result=$DB->add($map1);
			dump($_SESSION['headcollege']);
			$condition2['substuid']=$_SESSION['headnumber'];
			$map3=$DB->where($condition2)->find();
			$map2['csubjectid']=$map3['subjectid'];
			$map2['cheadname']=$_SESSION['headname'];
			$map2['cgroupid']=$_SESSION['hgroupnumber'];
			
			$result2=$DB2->add($map2);
			if($result>0){
			
			$this->success('自命题成功');}
			}
			
			
			
			
	}	
	
	public function result(){
			session_start();
			$DB=M('subject');
			$DB1=M('endchoose');
			$condition1['egroupid']=$_SESSION['hgroupnumber'];
			$list1=$DB1->where($condition1)->select();
			$DB2=M('rejchoose');
			$condition2['rejgroupid']=$_SESSION['hgroupnumber'];
			$list2=$DB2->where($condition2)->select();
			$DB3=M('choose');
			$condition3['cgroupid']=$_SESSION['hgroupnumber'];
			$list3=$DB3->where($condition3)->select();
			$condition['cgroupid']=$_SESSION['hgroupnumber'];
			$map=$DB3->where($condition)->find();
			$tnumber=$map['csubjectid'];
			$condition5['subjectid']=$tnumber;
			$list=$DB->where($condition5)->select();
			if($list1){
				$hello="选题通过";
				$this->assign('list',$list);}
			elseif($list2){
				$hello="选题没有通过";
				$this->assign('list',$list);}
			elseif($list3){
				$hello="正在审核中";
				$this->assign('list',$list);}
			else
				$hello="您还没有选题";
		
			$this->assign('hello',$hello);
			$this->display();
	}
	
	public function midreport()
	{
			session_start();
			$DB1=M('endchoose');
			$condition1['egroupid']=$_SESSION['hgroupnumber'];
			$list1=$DB1->where($condition1)->find();
			if($list1)
			{
				$id=$list1['esubjectid'];
				$DB=M('subject');
				$condition['subjectid']=$id;
				$map=$DB->where($condition)->find();
				$title=$map['subtitle'];
				$content=$_POST['content'];
				if($content){
				$DB2=M('midreport');
				$map1['midsubid']=$id;
				$map1['midgroupid']=$_SESSION['hgroupnumber'];
				$map1['midcontent']=$content;
				$map1['midtime']=date("Y-m-d H:i:s");
				$result=$DB2->add($map1);	
				$this->assign('title',$ttile);
				if($result>0){
				$this->assign("jumpUrl",__URL__.'/home');
				$this->success('中期报告提交成功');	
							}
				}
				else
					echo "<script language=\"JavaScript\">alert(\"报告不能为空\");self.location='mid_report';</script>"; 
			}
			else
				echo "<script language=\"JavaScript\">alert(\"您的选题还未通过\");self.location='result';</script>"; 
				
	}
	
	
	
	
	public function choosetitle()
	{
			session_start();
			$DB=M('subject');
			$this->redirect('show');
	}
	
	
	public function show()
	{		
			session_start();
			$DB=M('subject');
			$condition['subcoll']=$_SESSION['headcollege'];
			$condition['suborien']=$_SESSION['headorien'];
			$condition['subsubtype']="老师命题";
			$list=$DB->where($condition)->select();
			$this->assign('list',$list);
			$this->display();
	}
	
	public function choosesubject(){
			session_start();
			$DB=M('choose');
			$condition['cgroupid']=$_SESSION['hgroupnumber'];
			$map=$DB->where($condition)->select();
			if($map)
			{
				echo "<script language=\"JavaScript\">alert(\"已经选题或者自命题，请查看选题结果\");self.location='result';</script>"; 
			}
			else{
				$DB2=M('subject');
				$c=$_POST['choose'];
				$list['csubjectid']=$c;
				$list['cgroupid']=$_SESSION['hgroupnumber'];
				$list['cheadname']=$_SESSION['headname'];
				$list['choosetime']=date("Y-m-d H:i:s");
				$result=$DB->add($list);
					if($result>0){
					$this->assign("jumpUrl",__URL__.'/result');
					$this->success('选题成功');}

				}
	}	
	
	public function upload() {
				session_start();
				$DB=M('endchoose');
				$DB2=M('file');
				$DB3=M('subject');
				$condition['egroupid']=$_SESSION['hgroupnumber'];
				$result=$DB->where($condition)->find();
				if($result)
				{
					$condition2['fgroupid']=$_SESSION['hgroupnumber'];
					$result2=$DB2->where($condition2)->select();
					if($result2){
						echo "<script language=\"JavaScript\">alert(\"您已提交论文\");window.history.back(-1);</script>"; }
					else{
						$_SESSION['subjectid']=$result['esubjectid'];
						$condition3['subjectid']=$_SESSION['subjectid'];
						$result3=$DB3->where($condition3)->find();
						import('ORG.Net.UploadFile');
						$upload = new UploadFile();// 实例化上传类
						$upload->maxSize  = 3145728 ;// 设置附件上传大小
						$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg','txt','doc','docx','pdf','exe');// 设置附件上传类型
						$upload->savePath =  './Public/Uploads/';// 设置附件上传目录
						$_SESSION['path']=$upload->savePath;
						if(!$upload->upload()) {// 上传错误提示错误信息
							$this->error($upload->getErrorMsg());
						}
						else{// 上传成功
						//取得成功上传的文件信息
						$info = $upload->getUploadFileInfo();
						$model = M('file');
						//保存当前数据对象
						$data['file'] = $info[0]['savename'];
						$data['fgroupid']=$_SESSION['hgroupnumber'];
						$data['fheadname']=$_SESSION['headname'];
						$data['ftime'] = date("Y-m-d H:i:s");
						$data['ftitle']=$result3['subtitle'];
						$data['fteacherid']=$result3['subteacherid'];
						dump($data);
						$model->add($data);
						$this->assign("jumpUrl",__URL__.'/home');
						$this->success('上传成功！');
						}
					}
				}
				else{
				$this->assign("jumpUrl",__URL__.'/begin');
				$this->success('您还没有选题');	}
}

	public function show_report()
	{
			session_start();	
			$DB=M('file');
			$condition['fgroupid']=$_SESSION['hgroupnumber'];
			$result=$DB->where($condition)->find();
			if(!$result)
				$show='您还没有上传报告';
			else
			{
				$show=$result['file'];
			}
			$this->assign("show",$show);
			$this->display();
	}
	
	public function download()
	{
			$id=$_GET['id'];
			$file=D('file');
			$condition['fgroupid']=$id;
			$result=$file->where($condition)->find();
			$savename=$result['file'];
			dump($savename);
			$uploadpath='./Public/Uploads/';
			$filename=$uploadpath.$savename;
			import('ORG.Net.Http');
			http::download($filename);
	}
}
?>