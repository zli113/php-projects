<?php
// 本文档自动生成，仅供测试运行
class BackAction extends Action
{
    /**
    +----------------------------------------------------------
    * 默认操作
    +----------------------------------------------------------
    */
    function _initialize()
	{	
		 header('Content-Type:text/html; charset=utf-8');//防止出现乱码
		 if($_SESSION['type'])
		 {
		//	unset($_SESSION[C('USER_AUTH_KEY')]);
		//	unset($_SESSION);
		//	session_destroy();
		 }
	}	
	
	public function insert()
	{
		//session_start();
		$_SESSION['login']=false;
		$DB=D('headman');
		$hid=$_POST['hnumber'];
		$name=$_POST['hname'];
		$passwd1=$_POST['hpassword1'];
		$passwd2=$_POST['hpassword2'];
		$college=$_POST['hcollege'];
		$orien=$_POST['horien'];
		$phone=$_POST['hphone'];
		$mail=$_POST['hmail'];
		$sregip=getenv(REMOTE_ADDR);
		$hregtime=date("Y-m-d H:i:s");
		if(!$passwd1)
		{
		echo "<script language=\"JavaScript\">alert(\"密码不能为空\");self.location='reg';</script>"; 
		//$this->redirect('profile');
		}
		if($passwd1!=$passwd2)
		{
		echo "<script language=\"JavaScript\">alert(\"两次密码不同，请重新输入\");self.location='reg';</script>"; 
		}
		$map=array();
		$map['headnumber']=$hid;
		$list=$DB->where($map)->find();
		if($list)
		{
		echo "<script language=\"JavaScript\">alert(\"这个学号已经注册\");self.location='reg';</script>"; 
		//$this->redirect('reg');
		}
		else
		{
		$map1['headnumber']=$hid;
		$map1['headname']=$name;
		$map1['hpassword']=$passwd1;
		$map1['headcollege']=$college;
		$map1['headorien']=$orien;
		$map1['headphone']=$phone;
		$map1['headmail']=$mail;
		$map1['hregtime']=date("Y-m-d H:i:s");
		//dump($map1);
		$result=$DB->add($map1);
		if($result>0){
		$this->assign("jumpUrl",__URL__.'/index');
		$this->success('注册成功');}
		}

	}
	public function checkUser()
	{
		session_start();
		$select=$_POST['select'];

		if($select==='学生'){
					    $_SESSION['login']=false;
					    $DB=D('headman');
						$usernum=$_POST['number'];
						$pwd=$_POST['password'];
						 if (empty($usernum))
						{
						  echo "input your name:";
						  $this->redirect('Index/','',0,'');
						}
						 else if (empty($pwd))
						{
						 echo "input yor passwd:";
						  $this->redirect('Index/','',0,'');
						}
					//     else $this->redirect('Index/info','',0,'');
						 $map=array(); 
						 $map['headnumber']=$usernum;
						 $map['hpassword']=$pwd;
						 $list=$DB->where($map)->find();
							if(!$list)   
						  {
							 echo "<script language=\"JavaScript\">alert(\"用户名或密码错误\");self.location='index';</script>"; 
							 //$this->redirect('Index/','',0,''); 
						  } 
							else    
						   {
							//echo $list['numble'];
						   $_SESSION['hgroupnumber']=$list['hgroupnumber'];
						   $_SESSION['headnumber']=$list['headnumber'];
							 // echo $_SESSION['numble'];	
						   $_SESSION['hpassword']=$list['hpassword'];
						   //$_SESSION['SCOLLEGE']=$list['SCOLLEGE'];
						   $_SESSION['headname']=$list['headname'];
						   $_SESSION['headcollege']=$list['headcollege'];
						   $_SESSION['headorien']=$list['headorien'];
						   $_SESSION['headphone']=$list['headphone'];
						   $_SESSION['headmail']=$list['headmail'];
						   $_SESSION['hregtime']=$list['hregtime'];
						   $_SESSION['login']=true;
						   $_SESSION[C('USER_AUTH_KEY')]=$list['headname'];
						   $_SESSION['KIND']="同学";
						   $this->assign("jumpUrl",'__APP__/Index/home');
						   $this->success('success!');
						   }
						   
		}
		if($select==='教师')
		{
						$_SESSION['login']=false;
					    $DB=D('teacher');
						$usernum=$_POST['number'];
						$pwd=$_POST['password'];
						 if (empty($usernum))
						{
						  echo "input your name:";
						  $this->redirect('Index/','',0,'');
						}
						 else if (empty($pwd))
						{
						 echo "input yor passwd:";
						  $this->redirect('Index/','',0,'');
						}
					//     else $this->redirect('Index/info','',0,'');
						 $map=array(); 
						 $map['teacherid']=$usernum;
						 $map['teacherpwd']=$pwd;
						 $list=$DB->where($map)->find();
							if(!$list)   
						  {
							 echo "<script language=\"JavaScript\">alert(\"用户名或密码错误\");self.location='index';</script>"; 
							 //$this->redirect('Index/','',0,''); 
						  } 
							else    
						   {
						   $_SESSION['teacherid']=$list['teacherid'];
						   $_SESSION['teachername']=$list['teachername'];
						   $_SESSION['teacherpwd']=$list['teacherpwd'];
						   $_SESSION['teachercoll']=$list['teachercoll'];
						   $_SESSION['teacherori']=$list['teacherori'];
						   $_SESSION['teacherphone']=$list['teacherphone'];
						   $_SESSION['teachermail']=$list['teachermail'];
						   $_SESSION['tregtime']=$list['tregtime'];
						   $_SESSION['login']=true;
						   $_SESSION[C('USER_AUTH_KEY')]=$list['teachername'];
						   $_SESSION['KIND']="教师";
						   $this->assign("jumpUrl",'__APP__/Back/home');
						   $this->success('success!');
						   }
		}
	}
	//退出
	public function logout ()
	{
			if(isset($_SESSION[C('USER_AUTH_KEY')])) 
			{
			unset($_SESSION[C('USER_AUTH_KEY')]);
			unset($_SESSION);
			session_destroy();
			$this->redirect('index',array('cate_id'=>2),0,'byebye');

			}
			 else {echo "wrong"; $this->redirect('Index/','',0,'');}
	}
	
	public function info ()
	{
			 session_start();
			 $DB=M('teacher');
			 $condition['teacherid']=$_SESSION['teacherid'];
			 $list=$DB->where($condition)->select();
			 //dump($list);
			 $this->assign('list',$list);
			 $this->display();
	 }
	
	public function changeinformation()
	{
			session_start();
			$DB=M('teacher');
			$p=$_POST['teacherphone'];
			$q=$_POST['teachermail'];
			if(!$p)
				$data['teacherphone']=$_SESSION['teacherphone'];
			else{
				$data['teacherphone']=$p;
				$_SESSION['teacherphone']=$p;}
			if(!$q)
				$data['teachermail']=$_SESSION['teachermail'];
			else{
				$data['teachermail']=$q;
				$_SESSION['teachermail']=$q;}
			$data['teacherid']=$_SESSION['teacherid'];
			$result=$DB->save($data);
					if($result!==false){
						$this->assign("jumpUrl",__URL__.'/info');
						$this->success('修改成功');
						}
	}
	
	public function creat_subject()
	{
			session_start();
			$DB=M('subject');
			$title=$_POST['title'];
			$info=$_POST['info'];
			$condition['subtitle']=$title;
			$list=$DB->where($condition)->select();
			if($list)
				echo "<script language=\"JavaScript\">alert(\"该题目已经存在，请重新命题\");self.location='begin';</script>"; 
			else
			{
				$map['subteacherid']=$_SESSION['teacherid'];
				$map['subteachername']=$_SESSION['teachername'];
				$map['subtitle']=$title;
				$map['subcoll']=$_SESSION['teachercoll'];
				$map['suborien']=$_SESSION['teacherori'];
				$map['subinfo']=$info;
				$map['subsubtype']="老师命题";
				$result=$DB->add($map);
				if($result>0)
					$this->success('命题成功');
					
			}
	}
	
	public function havecreated()
	{
			session_start();
			$DB=M('subject');
			$condition['subteacherid']=$_SESSION['teacherid'];
			$list=$DB->where($condition)->select();
			if(!$list)
				echo "<script language=\"JavaScript\">alert(\"您还没有命题\");self.location='begin';</script>"; 
			else
			{
				$this->assign('list',$list);
				$this->display();
			}
	}
	
	public function delect_subject()
	{
			session_start();
			$DB=M('subject');
			$DB1=M('choose');
			$id=$_POST['checkbox'];
			$id_number=count($id);
			for($i=0;$i<$id_number;$i++){
				$condition1['csubjectid']=$id[$i];
				$map=$DB1->where($condition1)->select();
				if($map)
					echo "<script language=\"JavaScript\">alert(\"已经有学生选题，无法删除\");self.location='havecreated';</script>"; 
				else{
				$condition['subjectid']=$id[$i];
				$list=$DB->where($condition)->delete();}
				}
			if($list){
			$this->assign("jumpUrl",__URL__.'/havecreated');
			$this->success('删除成功');
			}
	}
	
	public function decide()
	{	
			session_start();
			$DB=M('subject');
			$DB2=M('choose');
			$DB3=M('headman');
			$condition['subteacherid']=$_SESSION['teacherid'];
			$list=$DB->where($condition)->select();
			$list_number=count($list);
			//dump($list);
			$this->assign('list',$list);
			$this->display();
	}
	
	public function decide_subject()
	{
			session_start();
			$DB=M('subject');
			$title=$_POST['title'];
			//dump($title);
			$_SESSION['title']=$title;
			$condition['subtitle']=$_SESSION['title'];
			$list=$DB->where($condition)->find();
			//dump($list);
			$_SESSION['titleid']=$list['subjectid'];
			$this->redirect('decide_student');
	}	
	
	public function decide_student()
	{
			session_start();
			$DB=M('choose');
			$DB2=M('headman');
			$DB3=M('subject');
			//dump($_SESSION['titleid']);
			$condition['csubjectid']=$_SESSION['titleid'];
			$list=$DB->where($condition)->select();
			$condition3['subjectid']=$_SESSION['titleid'];
			$list3=$DB3->where($condition3)->find();
			if($list3['subsubtype']=='自命题')
			{
				$show='该题目由学生命题';
				$showinfo=$list3['subinfo'];
				}
			$this->assign('show',$show);
			$this->assign('showinfo',$showinfo);
			$this->assign('list',$list);
			$this->display();
	}
	
	public function decide_end()
	{
			session_start();
			$DB=M('choose');
			$DB2=M('endchoose');
			$DB3=M('rejchoose');
			$DB5=M('headman');
			$condition['esubjectid']=$_SESSION['titleid'];
			$search=$DB2->where($condition)->select();
			if($search)
				echo "<script language=\"JavaScript\">alert(\"此题目已经选择过学生\");self.location='decide';</script>"; 
			else
			{
				$c=$_POST['choose'];
				$condition5['hgroupnumber']=$c;
				$list5=$DB5->where($condition5)->find();
				$headname=$list5['headname'];				
				$list['esubjectid']=$_SESSION['titleid'];
				$list['egroupid']=$c;
				$list['eheadname']=$headname;
				$list['endchoosetime']=date("Y-m-d H:i:s");
				$result=$DB2->add($list);
				
				$condition1['esubjectid']=$_SESSION['titleid'];
				$map1=$DB2->where($condition1)->find();
				$groupid=$map1['egroupid'];
				//dump($groupid);
				
				$condition2['csubjectid']=$_SESSION['titleid'];
				$map=$DB->where($condition2)->select();
				//dump($map);
				$n=count($map);
				for($i=0;$i<$n;$i++)
				{
					if($map[$i]['cgroupid']!=$groupid)
					{
						$reg['rejsubjectid']=$_SESSION['titleid'];
						$reg['rejgroupid']=$map[$i]['cgroupid'];
						$reg['rejtime']=date("Y-m-d H:i:s");
						$reject=$DB3->add($reg);
						$condition4['cgroupid']=$map[$i]['cgroupid'];
						$del=$DB->where($condition4)->delete();
					}
				}
				if($result>0)
				{
						$this->success('选择成功');
						$this->redirect('result');
				}
			}
	}
	
	public function result()
	{
			session_start();
			$DB1=M('endchoose');
			$DB2=M('subject');
			$DB3=M('headman');
			$condition2['subteacherid']=$_SESSION['teacherid'];
			$list2=$DB2->where($condition2)->select();
			$num2=count($list2);
			for($i=0;$i<$num2;$i++)
			{
				$map[$i]['title']=$list2[$i]['subtitle'];
				$condition['esubjectid']=$list2[$i]['subjectid'];
				$list=$DB1->where($condition)->find();
				if($list)
					$map[$i]['headman']=$list['eheadname'];
				$condition3['hgroupnumber']=$list['egroupid'];
				$list3=$DB3->where($condition3)->find();
				if($list3)
					$map[$i]['headphone']=$list3['headphone'];
			}
			$this->assign('map',$map);
			$this->display();
	}
	
	public function changesubject()
	{
			session_start();
			$DB=M('subject');
			$titleid=$_GET['id'];
			$condition['subjectid']=$titleid;
			$list=$DB->where($condition)->select();
			$this->assign('list',$list);
			$this->display();
	}
	
	public function changesubinfo()
	{
			session_start();
			$DB=M('subject');
			$info=$_POST['info'];
			$titleid=$_GET['pid'];
			//dump($titleid);
			if(!$info)
				echo "<script language=\"JavaScript\">alert(\"介绍不能为空\");window.history.back(-1);</script>";
			else
			{
				$condition['subjectid']=$titleid;
				$list=$DB->where($condition)->find();	
				//dump($list);
				$list['subinfo']=$info;
				$result=$DB->save($list);
				$this->assign("jumpUrl",__URL__.'/havecreated');
				$this->success('修改成功');
				
			}
	}
	
	public function mid_report()
	{	
			session_start();
			$DB=M('subject');
			$condition['subteacherid']=$_SESSION['teacherid'];
			$list=$DB->where($condition)->select();
			$this->assign('list',$list);
			$this->display();
	}
	
	public function comment()
	{
			session_start();
			$DB=M('midreport');
			$DB2=M('subject');
			$condition2['subtitle']=$_POST['title'];
			$result2=$DB2->where($condition2)->find();
			$titleid=$result2['subjectid'];
			$condition['midsubid']=$titleid;
			$_SESSION['titleid']=$titleid;
			$result=$DB->where($condition)->find();
			if(!$result)
				$show='该学生还没有提交中期报告';
			else
			{
				$show='';
				$content=$result['midcontent'];
				$this->assign('content',$content);
			}
			$this->assign('show',$show);
			$this->display();
	}
	
	public function submit_comment()
	{
			session_start();
			$DB=M('midreport');
			$content=$_POST['content'];
			$condition['midsubid']=$_SESSION['titleid'];
			$data['midcomment']=$content;
			$result=$DB->where($condition)->save($data);
			dump($result);
			if($result){
			$this->assign("jumpUrl",__URL__.'/home');
			$this->success('评论成功');
			}
	}
	
	public function view_comment()
	{
			session_start();
			$DB=M('subject');
			$condition['subteacherid']=$_SESSION['teacherid'];
			$list=$DB->where($condition)->select();
			$this->assign('list',$list);
			$this->display();	
	}
	
	public function view_comment2()
	{
			session_start();
			$DB=M('midreport');
			$DB2=M('subject');
			$condition2['subtitle']=$_POST['title'];
			$result2=$DB2->where($condition2)->find();
			$titleid=$result2['subjectid'];
			$condition['midsubid']=$titleid;
			$result=$DB->where($condition)->find();
			if($result)
			{
			$this->assign('list1',$result['midcontent']);
			$this->assign('list2',$result['midcomment']);
			$this->display();	
			}
				
	}
	public function download_report()
	{
			session_start();
			$DB=M('file');
			$condition['fteacherid']=$_SESSION['teacherid'];
			$list=$DB->where($condition)->select();
			$this->assign('list',$list);
			$this->display();
	}
	
	public function download()
	{
			$id=$_GET['id'];
			$file=D('file');
			$condition['fgroupid']=$id;
			$result=$file->where($condition)->find();
			$savename=$result['file'];
			dump($savename);
			$uploadpath='./Public/Uploads/';
			$filename=$uploadpath.$savename;
			import('ORG.Net.Http');
			http::download($filename);
	}
}
?>