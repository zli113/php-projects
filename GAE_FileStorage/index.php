<!--Author: Zeyu Li-->
<!--Author: A20304056-->
<?php
/*upload API*/
require_once 'google/appengine/api/cloud_storage/CloudStorageTools.php';
use google\appengine\api\cloud_storage\CloudStorageTools;

$options = [ 'gs_bucket_name' => 'zeyuli553hw3.appspot.com' ];
$upload_url = CloudStorageTools::createUploadUrl('/upload.php', $options);
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CS553 Assignment3</title>
<script language="javascript"  type="text/javascript">
function controlDIV()	//switch div-upload, check, find delete, list
{
	var ele=event.srcElement;
	if (ele.id=="upload_button")
	{
		document.getElementById("upload").style.visibility="visible";
		document.getElementById("check").style.visibility="hidden";
		document.getElementById("find").style.visibility="hidden";
		document.getElementById("delete").style.visibility="hidden";
		document.getElementById("list").style.visibility="hidden";
	}
	if (ele.id=="check_button")
	{
		document.getElementById("upload").style.visibility="hidden";
		document.getElementById("check").style.visibility="visible";
		document.getElementById("find").style.visibility="hidden";
		document.getElementById("delete").style.visibility="hidden";
		document.getElementById("list").style.visibility="hidden";
	}	
	if (ele.id=="find_button")
	{
		document.getElementById("upload").style.visibility="hidden";
		document.getElementById("check").style.visibility="hidden";
		document.getElementById("find").style.visibility="visible";
		document.getElementById("delete").style.visibility="hidden";
		document.getElementById("list").style.visibility="hidden";
	}
	if (ele.id=="delete_button")
	{
		document.getElementById("upload").style.visibility="hidden";
		document.getElementById("check").style.visibility="hidden";
		document.getElementById("find").style.visibility="hidden";
		document.getElementById("delete").style.visibility="visible";
		document.getElementById("list").style.visibility="hidden";
	}
	if (ele.id=="list_button")
	{
		document.getElementById("upload").style.visibility="hidden";
		document.getElementById("check").style.visibility="hidden";
		document.getElementById("find").style.visibility="hidden";
		document.getElementById("delete").style.visibility="hidden";
		document.getElementById("list").style.visibility="visible";
	}
	
} 
function getFilesname()	//show chosen files information
{
	var table = document.getElementById("d")
	var rowNum = table.rows.length;
     	for (i=0;i<rowNum;i++)
     	{
         	table.deleteRow(i);
         	rowNum=rowNum-1;
         	i=i-1;
     	}
	for(var i=0;i<document.getElementById('file').files.length;i++)
	{
		var tr = table.insertRow();
  		var td = tr.insertCell();
		td.innerText=document.getElementById('file').files[i].name;
		var temp1 = tr.insertCell();
		var temp2 = tr.insertCell();
		var td2 = tr.insertCell();
		td2.innerText=document.getElementById('file').files[i].size+" B";
		
	}
}
function whether_file()
{
	if(document.getElementById('file').files.length==0)
		alert("No selection files");
	
}
function whether_text()
{
	if(document.getElementById('filename').value="")
		alert("Please input file name");
	
}

</script> 
</head>
<body>

<div id="whole page">
	<div id="title" style="height:50px">
		<h1 align=center>CS 553 Assignment3</h1>
	</div>
	<div id="choices" style="text-align:center">
		<table align="center">
			<tr>
				<td style="text-align:center"><input id="upload_button" onclick="controlDIV()" type="button" value="Upload" style="width:100px;height:60px;font-size:20px"></td>			
				<td style="text-align:center"><input id="check_button" onclick="controlDIV()" type="button" value="Check" style="width:100px;height:60px;font-size:20px"></td>
				<td style="text-align:center"><input id="find_button" onclick="controlDIV()" type="button" value="Find" style="width:100px;height:60px;font-size:20px"></td>
				<td style="text-align:center"><input id="delete_button" onclick="controlDIV()" type="button" value="Delete" style="width:100px;height:60px;font-size:20px"></td>
				<td style="text-align:center"><input id="list_button" onclick="controlDIV()" type="button" value="List" style="width:100px;height:60px;font-size:20px"></td>
			</tr>
		</table>
	</div>
	<div style="height:20px">
	</div>
	<hr style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="80%" color=#987cb9 SIZE=3>
	<div style="position:relative;width:100%">
		<div id="upload" style="visibility:hidden;position:absolute;width:100%">
			<form action="<?php echo $upload_url; $_SESSION['start']=microtime(true);?>" method="post" enctype="multipart/form-data">
				<h2 align=center>Upload files here</h2>
				<table align=center border="1">
					<tr>
					<td><input type="file"  multiple id="file" name="file[]" onchange="getFilesname()"></td>
					</tr>	 
				<table>
				<div style="height:10px">
				</div>
				<div style="height:20px">
					<h2 align=center>Your selection files:</h2>
				</div>
				<div style="height:10px">
				</div>
				<div>
					<table id="d" align=center>	
					</table>
					<br>
					<br>	
					<center><input type="submit" name="Upload" value="Upload" style="width:100px;height:60px;font-size:20px"></center>
				</div>
			</form>
		</div>
		<div id="check" style="visibility:hidden;position:absolute;width:100%">
			<form action="check.php" method="post">
				<h2 align=center>Check files here</h2>
				<table align=center>
					<tr>
					<td>input filename here:</td>
					<td align=center><input type="text" name="filename"></td>
					</tr>
					<tr></tr><tr></tr>	 
				</table>
				<center><input type="submit" name="check" value="Check" onclick="whether_text()" style="width:90px;height:50px;font-size:20px"></center>
			</form>
		</div>
		<div id="find" style="visibility:hidden;position:absolute;width:100%">
			<form action="find.php" method="post">
				<h2 align=center>Find files here</h2>
				<table align=center>
					<tr>
					<td>input filename here:</td>
					<td align=center><input type="text" name="filename"></td>
					</tr>
					<tr></tr><tr></tr>	 
				</table>
				<center><input type="submit" name="find" value="Find" onclick="whether_text()" style="width:90px;height:50px;font-size:20px"></center>
			</form>
			<form action="findall.php" method="post">
			<center><input type="submit" name="find" value="FindAll" style="width:90px;height:50px;font-size:20px"></center>
			</form>

		</div>
		<div id="delete" style="visibility:hidden;position:absolute;width:100%">
			<form action="delete.php" method="post">
				<h2 align=center>Delete files here</h2>
				<table align=center>
					<tr>
					<td>input filename here:</td>
					<td align=center><input type="text" name="filename"></td>
					</tr>
					<tr></tr><tr></tr>	 
				</table>
				<center><input type="submit" name='deleteone' value="Delete" onclick="whether_text()" style="width:90px;height:50px;font-size:20px"></center>
			</form>
			<form action="deleteall.php" method="post">
			<center><input type="submit" name="delete" value="DeleteAll" style="width:90px;height:50px;font-size:20px"></center>
			</form>
		</div>
		<div id="list" style="visibility:hidden;position:absolute;width:100%">
			<form action="list.php" method="post">
				<h2 align=center>List files here</h2>
				<table align=center>
					<tr></tr><tr></tr>
					<tr align=center>
					<td><input type="submit" name='list' value="ListGCS" style="width:140px;height:50px;font-size:20px"></td>
					</tr> 
				</table>
			</form>	
		</div>
		
	</div>
<div>


</body>
</html>
