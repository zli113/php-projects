<!--Author: Zeyu Li-->
<!--Author: A20304056-->
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<div>
	<div style="height:50px">
		<h1 align=center>Upload Information</h1>
	<div>
	<hr style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="80%" color=#987cb9 SIZE=3>
	<div>
	<h3 align=center>
	<?php
		$_SESSION['end']=microtime(true);
		echo "All files are inserted into Google Cloud Storage. <br />";
		$count = count($_FILES['file']['name']);
		$memcache = new Memcache;
		for ($i = 0; $i < $count; $i++)
		{
			
			$filename=$_FILES['file']['name'][$i];
			$filesize=$_FILES['file']['size'][$i];
				/*if the size of file <=100KB, then add it to memcache*/
				if($filesize<=100*1024)
				{
					if($memcache->get($filename)!=false)
						echo $filename." already exit in memcache<br />";
					$filecontent=file_get_contents($_FILES['file']['tmp_name'][$i]);
					$start_time=microtime(true);
					/*add filename and filecontent to memcache*/
					$upload=$memcache->add($filename, $filecontent);
					$end_time=microtime(true);
					if($upload==true)
						echo $filename." add into memcache successful, time used ".($end_time-$start_time)." s<br />";
				}	
				/*rename files from tmp_name to name*/
				$options = ['gs' => ['Content-Type' => 'binary/octet-stream']];
				$ctx = stream_context_create($options);
				$originalname='gs://zeyuli553hw3.appspot.com/'.$_FILES['file']['name'][$i];
				if (false == rename($_FILES['file']['tmp_name'][$i], $originalname, $ctx)) 
				{
	  				die('Could not rename.');
				}	
		}
	?>
	</h3>
	<div>
		<!--back button-->
		<center><input type="button" value="Back" onclick ="location.href='/'" style="width:70px;height:30px;font-size:20px"><center>
	</div>
	</div>
</div>
</body>
</html>


	
