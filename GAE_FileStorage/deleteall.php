<!--Author: Zeyu Li-->
<!--Author: A20304056-->
<!--
<?php
class multiplethreads extends Thread
{
	public function run()
	{
		do php code;
	}
}
$threads=[];
for($i=0;$i<3;$i++)
{
	$threads[$i]=new multiplethreads;
	$threads[$i]->start();
}
for($i=0;$i<3;$i++)
{
	$threads[$i]->join();
}
?>
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<div>
	<div style="height:50px">
		<h1 align=center>Delete Information</h1>
	<div>
	<hr style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="80%" color=#987cb9 SIZE=3>
	<div>
	<h3 align=center>
		<?php
			$memcache = new Memcache;
			/*delete all files from memcache*/
			$result=$memcache->flush();
			if($result!=false)
				echo "All files are deleted successful from memcache<br />";
			/*get GCS url*/
			$dir="gs://zeyuli553hw3.appspot.com/";
			if(is_dir($dir))
			{
				if ($dh=opendir($dir)) 
				{
					$startall=microtime(true);
					/*traversal files in GCS*/
					while (($file=readdir($dh))!== false)
					{
						$filename='gs://zeyuli553hw3.appspot.com/'.$file;
						$start=microtime(true);
						/*delete file from GCS*/
            					unlink($filename);
						$end=microtime(true);
						echo $file." is deleted for ".($end-$start)." s<br />";
        				}
					$endall=microtime(true);
					echo "All files are deleted successful from Google Cloud Storage, time used ".($endall-$startall)." s<br />";
				}
			}
		?>
	</h3>
	<div>
		<!--back button-->
		<center><input type="button" value="Back" onclick ="location.href='/'" style="width:70px;height:30px;font-size:20px"><center>
	</div>
	</div>
</div>
</body>
</html>
