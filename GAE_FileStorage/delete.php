<!--Author: Zeyu Li-->
<!--Author: A20304056-->
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<div>
	<div style="height:50px">
		<h1 align=center>Delete Information</h1>
	<div>
	<!--deviding line-->
	<hr style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="80%" color=#987cb9 SIZE=3>
	<div>
	<h3 align=center>
		<?php
			$filename=$_POST['filename'];
			$memcache = new Memcache;
			/*delete file from memcache*/
			$result=$memcache->delete($filename);
			if($result!=false)
			{
				echo $filename." is deleted successful from memcache<br />";
			}
			/*delete file from GCS*/
			$file='gs://zeyuli553hw3.appspot.com/'.$filename;
			if (file_exists($file))
			{
				unlink($file);
				echo "The file $file is deleted successful from Google Cloud Storage.";
			} 	
			else 
    				echo "The file $filename does not exist";	
		?>
	</h3>
	<div>
		<!--back button-->
		<center><input type="button" value="Back" onclick ="location.href='/'" style="width:70px;height:30px;font-size:20px"><center>
	</div>
	</div>
</div>
</body>
</html>
