<!--Author: Zeyu Li-->
<!--Author: A20304056-->
<!--
<?php
class multiplethreads extends Thread
{
	public function run()
	{
		do php code;
	}
}
$threads=[];
for($i=0;$i<3;$i++)
{
	$threads[$i]=new multiplethreads;
	$threads[$i]->start();
}
for($i=0;$i<3;$i++)
{
	$threads[$i]->join();
}
?>
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<div>
	<div style="height:50px">
		<h1 align=center>Find Information</h1>
	<div>
	<hr style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="80%" color=#987cb9 SIZE=3>
	<div>
	<h3 align=center>
		<?php
			require_once 'google/appengine/api/cloud_storage/CloudStorageTools.php';
			use google\appengine\api\cloud_storage\CloudStorageTools;
			/*get GCS url*/
			$dir="gs://zeyuli553hw3.appspot.com/";
			if(is_dir($dir))
			{
				if ($dh=opendir($dir)) 
				{
					$startall=microtime(true);
					/*traversal files in GCS*/
					while (($file=readdir($dh))!== false)
					{
						$start=microtime(true);
						$filename='gs://zeyuli553hw3.appspot.com/'.$file;
						/*get file content*/
						$filecontent=file_get_contents($filename);
						/*create public access url for each file*/
						$object_public_url = CloudStorageTools::getPublicUrl($filename, false);
						echo $file." is found, the download link is ";
						echo "<a href=$object_public_url>Download</a><br />";
						echo "The download is unavailable if files are not set to be shared public in GCS. But actually it is read from GCS.<br />";
						echo "You can use these code to show their contents in textarea.<br />";
						/*if you want, you can enable next code, and show file content in the textarea. But not recommend!!!*/
						echo "<!--<textarea rows=10 cols=100>filecontent</textarea>--><br />";
        				}
					$endall=microtime(true);		
					echo "All files are found successful from Google Cloud Storage, time used ".($endall-$startall)." s<br />";
				}
			}
		?>
	</h3>
	<div>
		<!--back button-->
		<center><input type="button" value="Back" onclick ="location.href='/'" style="width:70px;height:30px;font-size:20px"><center>
	</div>
	</div>
</div>
</body>
</html>
