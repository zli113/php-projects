<!--Author: Zeyu Li-->
<!--Author: A20304056-->
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<div>
	<div style="height:50px">
		<h1 align=center>Find Information</h1>
	<div>
	<hr style="FILTER: alpha(opacity=100,finishopacity=0,style=3)" width="80%" color=#987cb9 SIZE=3>
	<div>
	<h3 align=center>
	<?php
		/*get filename by $_POST*/
		$filename=$_POST['filename'];
		$memcache = new Memcache;
		/*check a file in memcache*/
		$result=$memcache->get($filename);
		if($result!=false)
		{
			echo $filename." find in memcache<br />";
			echo "The content of ".$filename." is: ";
		}
		else
		{
			/*check a file in GCS*/
			$file='gs://zeyuli553hw3.appspot.com/'.$filename;
			if (file_exists($file))
			{
				$start=microtime(true);
				/*get file content*/
				$result=file_get_contents($file);
				$end=microtime(true);
				echo "The file $filename exists in Google Cloud Storage, time used ".($end-$start)." s<br />";
				echo "The content of ".$filename." is: ";
			}
			else
				echo "The file $filename doesn't exit!<br />";
		}		
	?>
	</h3>
	<!--file content textarea-->
	<center><textarea rows="50" cols="100"> <?php echo $result?></textarea></center>
	<div style="height:20px">
	</div>
	<div>
		<!--back button-->
		<center><input type="button" value="Back" onclick ="location.href='/'" style="width:70px;height:30px;font-size:20px"><center>
	</div>
	</div>
</div>
</body>
</html>
